**Notes on ep_nuclearpp_v4 and load_generator scripts**

**\Disciplines\EP\selenium\ep_nuclearpp_v4 will excercise the FISHNet gui by creating 1 to 325 map points (of the EP discipline, nuclear powerplant facilty) and posting to the midb automation api.**

1. You must have 2 valid pki certs (one to access fishnet and the other to post to the midb automation api).
2. This requires chrome version 67 or later to run.
3. Install webdriver for google chrome from here https://sites.google.com/a/chromium.org/chromedriver/downloads
4. ** Update the literal 'driver = webdriver.Chrome('C:/Users/Mlynch/Downloads/chromedriver_win32/chromedriver')' to point to where you installed the webriver **
5. Follow the instructions here to setup chrome to handle certs correctly:
	https://opensgi.atlassian.net/wiki/spaces/MIDB/pages/709591107/Setting+Chrome+browser+to+auto+accept+and+choose+correct+PKI+certs
6. Install modules `pip install -r .\requirements.txt` (assuming a venv setup)
7. To run, type: `python ep_nuclearpp_v4.py` and follow the prompt.  Errors on exit option can be ignored.

**Load_generator_v6 includes the schema changes for MIDB NPW version 2.7.5**

**All scripts assume you are running python 3.6**

**load_generator_v6 will generate 1 to 325 map points (of the EP discipline, nuclear powerplant facilty) and post to the midb automation api's directly.**

1. Install modules `pip install -r .\requirements.txt`
2. (On Windows) Copy the cacert.pem file to the intended python enviroments `...\lib\site-packages\certifi\`
3. (On Mac) Copy the cacert.pem file to the repo's virtual environment location `scrapyard/venv/lib/python3.7/site-packages/certifi/`
4. To run, type: `python loadGeneratorv6.py` and follow the prompts. 
5. In addition to posting the geojson to the api, This will also create a file with the geojson called "lgen.json" of the data created for reference.  It will be over written each time the script is run.
6. Note: loadGeneratorv6 will use the supplied BBpem.pem file.

**load_generator_v6_static will generate 1 to 100000001 map points (of the EP discipline, nuclear powerplant facilty) in geojson file named lgen.json for manual submission.  Used for creating large test payloads.**

1. to run, type: `python loadGeneratorv6_static.py` and follow the prompt.
2. Note: loadGeneratorv6_static will not use any cert files as it's not needed.

---

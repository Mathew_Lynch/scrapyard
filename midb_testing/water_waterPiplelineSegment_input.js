{
	"features": [{
			"jobId": "1183",
			"geometry": {
				"coordinates": [37.6403994396549, 55.71328117525703],
				"type": "Point"
			},
			"id": "waterpipelinesegment.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "WATER",
				"oper_status": "OPR",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-02T08:49:00.000Z",
				"cc": "RS",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"ff_width_eval": "1",
				"control_mark": "IM",
				"feature_type": "waterpipelinesegment",
				"fac_name": "Danilov WPS MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-02T08:49:00.000Z",
				"review_date": "2018-08-02T08:49:00.000Z",
				"eval": "1",
				"ff_length_um": "M",
				"category": "43160",
				"ff_length_eval": "1",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

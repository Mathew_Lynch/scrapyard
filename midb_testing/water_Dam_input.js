{
 "features": [{
   "jobId": "1111",
   "geometry": {
    "coordinates": [30.73986285390312, 111.26520932366357],
    "type": "Point"
   },
   "id": "dams.undefined",
   "type": "Feature",
   "properties": {
    "accessible": "CLR",
    "coord_deriv": "AG",
    "prod_lvl_cap": "B",
    "activity": "AAC",
    "record_status": "A",
    "domain_lvl": "CO",
    "ff_width": "500",
    "ff_width_um": "M",
    "discipline": "WATER",
    "oper_status": "U",
    "spillway_height": "100",
    "release_mark": "BW",
    "coord": "30.82555556, 11.02472222",
    "last_chg_userid": "osgi",
    "datetime_created": "2018-07-24T15:30:00.000Z",
    "cc": "CH",
    "codeword": "0",
    "role_type": "LOCL",
    "class_lvl": "U",
    "ff_width_eval": "1",
    "control_mark": "IM",
    "feature_type": "dams",
    "fac_name": "Sandouping DAM MLTEST4",
    "condition": "Z",
    "datetime_last_chg": "2018-07-24T15:30:00.000Z",
    "review_date": "2018-07-24T15:30:00.000Z",
    "eval": "1",
    "ff_length_um": "M",
    "surface_level_height": "125",
    "second_name": "N/A",
    "category": "43910",
    "ff_length_eval": "1",
    "ff_length": "500"
   },
   "PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
  }
 ],
 "type": "FeatureCollection"
}



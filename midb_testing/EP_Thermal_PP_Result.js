{
	"Feature": {
		"FacilityComposite": {
			"FacTie": [{
					"TieFromSk": "-1",
					"ResProd": "DJ",
					"DatetimeCreated": "20180727110200",
					"DomainLvl": "CO",
					"DeclassOnDate": "20340107",
					"TieToSk": "-1",
					"TieToEntity": "FAC",
					"FacTieSk": "-1",
					"DeclassOn": "D",
					"ClassLvl": "U",
					"ProdLvlCap": "B",
					"Codeword": "0",
					"DatetimeLastChg": "20180727110200",
					"Assoc": "I  B",
					"LastChgUserid": "osgi",
					"Eval": "6",
					"RecordStatus": "A"
				}
			],
			"Status": "SUCCESS",
			"jobId": "1121",
			"FacForm": {
				"ResProd": "DJ",
				"DatetimeCreated": "20180727110200",
				"DomainLvl": "CO",
				"FacSk": "-1",
				"DeclassOnDate": "20340107",
				"FacFormSk": "-1",
				"DeclassOn": "D",
				"ClassLvl": "U",
				"ProdLvlCap": "B",
				"AzimuthRef": "TRU",
				"Codeword": "0",
				"DatetimeLastChg": "20180727110200",
				"ProducerDatetimeLastChg": "20180514082054",
				"LastChgUserid": "osgi",
				"Eval": "6",
				"RecordStatus": "A"
			},
			"Error": "",
			"Facility": {
				"Cc": "CH",
				"FacName": "SHI SAOBIAN TPP MLTST1",
				"DeclassManRev": false,
				"Category": "42100",
				"ReviewDate": "20180727",
				"CoordDatum": "WGE",
				"DatetimeCreated": "20180727110200",
				"DomainLvl": "CO",
				"Activity": "AAC",
				"CoordDeriv": "AG",
				"DeclassOnDate": "20340107",
				"DeclassOn": "D",
				"ProducerDatetimeLastChg": "20180514082054",
				"LastChgUserid": "osgi",
				"Eval": "6",
				"CoordDatetime": "20180514082054",
				"RecordStatus": "A",
				"Condition": "U",
				"Allegiance": "WW",
				"ResProd": "DJ",
				"FacSk": "99998000009040",
				"OperStatus": "U",
				"CoordBasis": "E",
				"Coord": "371922948N1053547235E",
				"ClassLvl": "U",
				"ProdLvlCap": "B",
				"Codeword": "0",
				"DatetimeLastChg": "20180727110200"
			}
		}
	}
}

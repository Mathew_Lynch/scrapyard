{
	"Feature": {
		"FacilityComposite": {
			"FacTie": [{
					"TieFromSk": "-1",
					"ResProd": "DJ",
					"DatetimeCreated": "20180801090100",
					"DomainLvl": "CO",
					"DeclassOnDate": "20340107",
					"TieToSk": "-1",
					"TieToEntity": "FAC",
					"FacTieSk": "-1",
					"DeclassOn": "D",
					"ClassLvl": "U",
					"ProdLvlCap": "C",
					"Codeword": "0",
					"DatetimeLastChg": "20180801090100",
					"LastChgUserid": "bigbear2",
					"Assoc": "I  B",
					"Eval": "6",
					"RecordStatus": "A"
				}
			],
			"Status": "SUCCESS",
			"jobId": "1155",
			"FacForm": {
				"ResProd": "DJ",
				"DatetimeCreated": "20180801090100",
				"DomainLvl": "CO",
				"FacSk": "-1",
				"DeclassOnDate": "20340107",
				"FacFormSk": "-1",
				"DeclassOn": "D",
				"ClassLvl": "U",
				"Length": "500",
				"ProdLvlCap": "C",
				"AzimuthRef": "TRU",
				"Codeword": "0",
				"DatetimeLastChg": "20180801090100",
				"LengthUm": "M",
				"WidthEval": "6",
				"ProducerDatetimeLastChg": "20180514082054",
				"WidthUm": "M",
				"LastChgUserid": "bigbear2",
				"Eval": "6",
				"Width": "500",
				"RecordStatus": "A",
				"LengthEval": "6"
			},
			"Error": "",
			"Facility": {
				"Cc": "CH",
				"FacName": "Daqang OilField MLTST2",
				"DeclassManRev": false,
				"Category": "15100",
				"ReviewDate": "20180801",
				"CoordDatum": "WGE",
				"DatetimeCreated": "20180801090100",
				"DomainLvl": "CO",
				"Activity": "AAC",
				"CoordDeriv": "AG",
				"DeclassOnDate": "20340107",
				"DeclassOn": "D",
				"ProducerDatetimeLastChg": "20180514082054",
				"LastChgUserid": "bigbear2",
				"Eval": "6",
				"CoordDatetime": "20180514082054",
				"RecordStatus": "A",
				"Condition": "U",
				"ResProd": "DJ",
				"Allegiance": "WW",
				"FacSk": "99998000009088",
				"OperStatus": "U",
				"CoordBasis": "E",
				"Coord": "463256822N1245855273E",
				"ClassLvl": "U",
				"ProdLvlCap": "C",
				"Codeword": "0",
				"DatetimeLastChg": "20180801090100"
			}
		}
	}
}

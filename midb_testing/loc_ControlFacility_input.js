{
	"features": [{
			"jobId": "1190",
			"geometry": {
				"coordinates": [128.1363012523191, 38.66503522127869],
				"type": "Point"
			},
			"id": "loc_control_facility.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"ff_height_eval": "6",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"vertical_clearance": "10",
				"oper_status": "OPR",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-02T08:49:00.000Z",
				"ff_height_um": "M",
				"span_length_um": "M",
				"cc": "KN",
				"vertical_clearance_eval": "6",
				"span_qty": "2",
				"codeword": "0",
				"role_type": "LOCL",
				"class_rating": "U",
				"vertical_clearance_um": "M",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "loc_control_facility",
				"fac_name": "DPRK LCF MLTST1",
				"span_length": "10",
				"condition": "U",
				"datetime_last_chg": "2018-08-02T08:49:00.000Z",
				"review_date": "2018-08-02T08:49:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "47700",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

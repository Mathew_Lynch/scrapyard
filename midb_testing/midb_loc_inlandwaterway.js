{
	"features": [{
			"jobId": "1065",
			"geometry": {
				"coordinates": [128.905, 37.79138889],
				"type": "Point"
			},
			"id": "inlandwaterway_facility.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"depth_max": "120",
				"oper_status": "U",
				"release_mark": "BW",
				"datetime_created": "2018-07-23T14:56:00.000Z",
				"cc": "KS",
				"codeword": "0",
				"role_type": "U",
				"depth_um": "M",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "inlandwaterway_facility",
				"depth_eval": "3",
				"fac_name": "Heogyun Canal MLTEST4",
				"condition": "U",
				"datetime_last_chg": "2018-07-23T14:56:00.000Z",
				"review_date": "2018-07-23T14:56:00.000Z",
				"depth": "125",
				"eval": "6",
				"ff_length_um": "M",
				"category": "47100",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "Mathew Lynch d16"
		}
	],
	"type": "FeatureCollection"
}

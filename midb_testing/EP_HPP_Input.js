{
	"features": [{
			"jobId": "1143",
			"geometry": {
				"coordinates": [100.91853887603845, 36.12222823964678],
				"type": "Point"
			},
			"id": "hydropowerplants.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"discipline": "EP",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-07-31T09:01:00.000Z",
				"scada_comm_medium": "0",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"hpptype": "2",
				"class_lvl": "U",
				"control_mark": "IM",
				"feature_type": "hydropowerplants",
				"fac_name": "Laxiwazhen HPP MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-07-31T09:01:00.000Z",
				"review_date": "2018-07-31T09:01:00.000Z",
				"eval": "6",
				"placement": "ACHB",
				"category": "42220"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

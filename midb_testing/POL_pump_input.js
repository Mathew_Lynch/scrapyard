{
	"features": [{
			"jobId": "1168",
			"geometry": {
				"coordinates": [93.68435798921156, 19.36020108885594],
				"type": "Point"
			},
			"id": "pump.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "C",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "POL",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-01T09:01:00.000Z",
				"cc": "CH",
				"codeword": "0",
				"role_type": "NATN",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "pump",
				"fac_name": "Sino-Myanmar Pump station MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-01T09:01:00.000Z",
				"review_date": "2018-08-01T09:01:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "21510",
				"ff_length_eval": "6",
				"relative_ranking": "500",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

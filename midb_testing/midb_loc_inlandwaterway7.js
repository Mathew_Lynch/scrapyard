{
	"features": [{
			"jobId": "1070",
			"geometry": {
				"coordinates": [37.79114968770572, 128.90622719803034],
				"type": "Point"
			},
			"id": "inlandwaterway_facility.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"depth_max": "120",
				"oper_status": "OPR",
				"release_mark": "BW",
				"coord": "37.7913888, 128.9050000",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-24T10:43:00.000Z",
				"cc": "KS",
				"codeword": "0",
				"role_type": "LOCL",
				"depth_um": "M",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "inlandwaterway_facility",
				"depth_eval": "6",
				"fac_name": "Heogyun Canal MLTEST7",
				"condition": "U",
				"datetime_last_chg": "2018-07-24T10:43:00.000Z",
				"review_date": "2018-07-24T10:43:00.000Z",
				"depth": "125",
				"eval": "6",
				"ff_length_um": "M",
				"category": "47100",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=Mathew Lynch d16, OU=Engineering, O=BigBear.io, L=Reston, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

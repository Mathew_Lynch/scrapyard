{
	"features": [{
			"jobId": "1153",
			"geometry": {
				"coordinates": [113.26218889894378, 21.87657430920699],
				"type": "Point"
			},
			"id": "riserpoles.undefined",
			"type": "Feature",
			"properties": {
				"create_userid": "bigbear2",
				"control_mark_att": "IM",
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"class_lvl_geo": "U",
				"discipline": "EP",
				"codeword_geo": "0",
				"admin_userid": "bigbear2",
				"release_mark_att": "BW",
				"control_mark_geo": "IM",
				"class_lvl_att": "U",
				"domain_lvl_geo": "CO",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-31T08:52:00.000Z",
				"codeword_att": "0",
				"scada_comm_medium": "0",
				"cc": "CH",
				"role_type": "LOCL",
				"domain_lvl_att": "CO",
				"geo_source": "CI",
				"feature_type": "riserpoles",
				"fac_name": "RiserPol CN MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-31T08:52:00.000Z",
				"review_date": "2018-08-31T08:52:00.000Z",
				"eval": "6",
				"placement": "ACHB",
				"admin_datetime_last_chg": "2018-08-31T08:52:00.000Z",
				"release_mark_geo": "BW",
				"source_date": "2018-08-31T08:52:00.000Z"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

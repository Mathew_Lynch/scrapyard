{
	"features": [{
			"jobId": "1138",
			"geometry": {
				"coordinates": [113.31924032573555, 23.10909141060015],
				"type": "Point"
			},
			"id": "radio_comm.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"ff_height_eval": "1",
				"record_status": "A",
				"domain_lvl": "CO",
				"discipline": "C4I",
				"oper_status": "OPR",
				"release_mark": "BW",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-30T13:10:00.000Z",
				"ff_height_um": "100FT",
				"cc": "CH",
				"ff_height": "1982",
				"codeword": "0",
				"role_type": "NATN",
				"class_lvl": "U",
				"control_mark": "IM",
				"feature_type": "radio_comm",
				"fac_name": "Canton Tower TropRC MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-07-30T13:10:00.000Z",
				"review_date": "2018-07-30T13:10:00.000Z",
				"eval": "6",
				"category": "41130"
			},
			"PKI_SUBJECT_DN": "CN=Mathew Lynch d16, OU=Engineering, O=BigBear.io, L=Reston, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

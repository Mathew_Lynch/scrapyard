{
	"features": [{
			"jobId": "1122",
			"geometry": {
				"coordinates": [47.50047880346007, 30.399182220048267],
				"type": "Point"
			},
			"id": "pol_vulnerablepoint.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "C",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "POL",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-27T12:02:00.000Z",
				"cc": "IZ",
				"codeword": "0",
				"role_type": "LOCL",
				"class_rating": "U",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "pol_vulnerablepoint",
				"fac_name": "Ramala Oil Field VP MLTST1",
				"condition": "Z",
				"datetime_last_chg": "2018-07-27T12:02:00.000Z",
				"review_date": "2018-07-27T12:02:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "21540",
				"ff_length_eval": "6",
				"relative_ranking": "500",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

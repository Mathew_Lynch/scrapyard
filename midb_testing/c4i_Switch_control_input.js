{
	"features": [{
			"jobId": "1139",
			"geometry": {
				"coordinates": [115.66157611719706, 37.043763323517304],
				"type": "Point"
			},
			"id": "switch_control.undefined",
			"type": "Feature",
			"properties": {
				"cc": "CH",
				"coord_deriv": "AG",
				"prod_lvl_cap": "C",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "NATN",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "C4I",
				"oper_status": "U",
				"control_mark": "IM",
				"feature_type": "switch_control",
				"fac_name": "Qinghe SC General MLTST2",
				"condition": "U",
				"datetime_last_chg": "2018-07-30T15:20:00.000Z",
				"review_date": "2018-07-30T15:20:00.000Z",
				"release_mark": "BW",
				"eval": "6",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-07-30T15:20:00.000Z",
				"category": "41400"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

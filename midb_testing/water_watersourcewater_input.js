{
	"features": [{
			"jobId": "1114",
			"geometry": {
				"coordinates": [128.7122722992764, 37.60282050230211],
				"type": "Point"
			},
			"id": "watersource.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "WATER",
				"oper_status": "U",
				"release_mark": "BW",
				"aquifer_name": "SUHA 2-GIL",
				"last_chg_userid": "osgi",
				"sourcetype": "4",
				"datetime_created": "2018-07-26T10:03:00.000Z",
				"cc": "KS",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"ff_width_eval": "1",
				"control_mark": "IM",
				"feature_type": "watersource",
				"fac_name": "SUHA 2-GIL MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-07-26T10:03:00.000Z",
				"review_date": "2018-07-26T10:03:00.000Z",
				"eval": "1",
				"ff_length_um": "M",
				"category": "43143",
				"ff_length_eval": "1",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

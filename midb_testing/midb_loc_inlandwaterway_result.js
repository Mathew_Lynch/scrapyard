{
	"Feature": {
		"FacilityComposite": {
			"FacTie": [{
					"TieFromSk": "-1",
					"ResProd": "DJ",
					"DatetimeCreated": "20180723145600",
					"DomainLvl": "CO",
					"DeclassOnDate": "20340107",
					"TieToSk": "-1",
					"TieToEntity": "FAC",
					"FacTieSk": "-1",
					"DeclassOn": "D",
					"ClassLvl": "U",
					"ProdLvlCap": "B",
					"Codeword": "0",
					"DatetimeLastChg": "20180723145600",
					"Assoc": "I  B",
					"LastChgUserid": "osgi",
					"Eval": "6",
					"RecordStatus": "A"
				}
			],
			"Status": "FAILED",
			"jobId": "1065",
			"FacForm": {
				"ResProd": "DJ",
				"DatetimeCreated": "20180723145600",
				"DomainLvl": "CO",
				"FacSk": "-1",
				"DeclassOnDate": "20340107",
				"FacFormSk": "-1",
				"DeclassOn": "D",
				"ClassLvl": "U",
				"Length": "500",
				"ProdLvlCap": "B",
				"AzimuthRef": "TRU",
				"Codeword": "0",
				"DatetimeLastChg": "20180723145600",
				"LengthUm": "M",
				"WidthEval": "6",
				"ProducerDatetimeLastChg": "20180514082054",
				"WidthUm": "M",
				"LastChgUserid": "osgi",
				"Eval": "6",
				"Width": "500",
				"RecordStatus": "A",
				"LengthEval": "6"
			},
			"Error": " Schema errors (mandatory field/size) for Installation/Facility [NEW] , for fields : Coordinate Field format error for Installation/Facility [NEW] for field Coordinate - rule: [00-90][00-59][00-59][000-999] Latitude degrees, minutes, seconds, thousands of seconds, [NS] hemisphere, [000-180][00-59][00-59][000-999] Longitude degrees, minutes, seconds, thousands of seconds, [EW] Hemisphere ",
			"Facility": {
				"Cc": "KS",
				"FacName": "Heogyun Canal MLTEST4",
				"DeclassManRev": false,
				"Category": "47100",
				"ReviewDate": "20180723",
				"CoordDatum": "WGE",
				"DatetimeCreated": "20180723145600",
				"DomainLvl": "CO",
				"Activity": "AAC",
				"CoordDeriv": "AG",
				"DeclassOnDate": "20340107",
				"DeclassOn": "D",
				"ProducerDatetimeLastChg": "20180514082054",
				"LastChgUserid": "osgi",
				"Eval": "6",
				"CoordDatetime": "20180514082054",
				"RecordStatus": "A",
				"Condition": "U",
				"Allegiance": "WW",
				"ResProd": "DJ",
				"FacSk": "-1",
				"OperStatus": "U",
				"CoordBasis": "E",
				"Coord": "1285418000N0374729000E",
				"ClassLvl": "U",
				"ProdLvlCap": "B",
				"Codeword": "0",
				"DatetimeLastChg": "20180723145600"
			}
		}
	}
}

{
	"Feature": {
		"FacilityComposite": {
			"FacTie": [{
					"TieFromSk": "-1",
					"ResProd": "DJ",
					"DatetimeCreated": "20180831085200",
					"DeclassOnDate": "20340107",
					"TieToSk": "-1",
					"TieToEntity": "FAC",
					"FacTieSk": "-1",
					"DeclassOn": "D",
					"ProdLvlCap": "B",
					"DatetimeLastChg": "20180831085200",
					"LastChgUserid": "bigbear2",
					"Assoc": "I  B",
					"Eval": "6",
					"RecordStatus": "A"
				}
			],
			"Status": "FAILED",
			"jobId": "1153",
			"FacForm": {
				"ResProd": "DJ",
				"DatetimeCreated": "20180831085200",
				"FacSk": "-1",
				"DeclassOnDate": "20340107",
				"FacFormSk": "-1",
				"DeclassOn": "D",
				"ProdLvlCap": "B",
				"AzimuthRef": "TRU",
				"DatetimeLastChg": "20180831085200",
				"ProducerDatetimeLastChg": "20180514082054",
				"LastChgUserid": "bigbear2",
				"Eval": "6",
				"RecordStatus": "A"
			},
			"Error": " Cannot determine Responsible Producer - please fill in a Category",
			"Facility": {
				"Condition": "U",
				"Cc": "CH",
				"FacName": "RiserPol CN MLTST1",
				"DeclassManRev": false,
				"ReviewDate": "20180831",
				"CoordDatum": "WGE",
				"ResProd": "DJ",
				"Allegiance": "WW",
				"DatetimeCreated": "20180831085200",
				"FacSk": "-1",
				"Activity": "AAC",
				"CoordDeriv": "AG",
				"DeclassOnDate": "20340107",
				"CoordBasis": "E",
				"Coord": "215235668N1131543880E",
				"DeclassOn": "D",
				"ProdLvlCap": "B",
				"DatetimeLastChg": "20180831085200",
				"ProducerDatetimeLastChg": "20180514082054",
				"LastChgUserid": "bigbear2",
				"Eval": "6",
				"CoordDatetime": "20180514082054",
				"RecordStatus": "A"
			}
		}
	}
}

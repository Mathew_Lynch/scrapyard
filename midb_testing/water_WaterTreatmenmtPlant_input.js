{
	"features": [{
			"jobId": "1174",
			"geometry": {
				"coordinates": [117.67666017740856, 38.99378588522305],
				"type": "Point"
			},
			"id": "watertreatmentplant.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "WATER",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-02T08:49:00.000Z",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"ff_width_eval": "1",
				"control_mark": "IM",
				"feature_type": "watertreatmentplant",
				"fac_name": "Tianjin WTP MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-02T08:49:00.000Z",
				"review_date": "2018-08-02T08:49:00.000Z",
				"eval": "1",
				"ff_length_um": "M",
				"length_dm": "500",
				"category": "43110",
				"ff_length_eval": "1",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

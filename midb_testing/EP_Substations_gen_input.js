{
	"features": [{
			"jobId": "1116",
			"geometry": {
				"coordinates": [108.31436890440436, 22.80547788866054],
				"type": "Point"
			},
			"id": "substations.undefined",
			"type": "Feature",
			"properties": {
				"cc": "CH",
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "LOCL",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "EP",
				"oper_status": "U",
				"control_mark": "IM",
				"feature_type": "substations",
				"fac_name": "Guangxi substation MLTST1",
				"condition": "Z",
				"release_mark": "BW",
				"datetime_last_chg": "2018-07-26T15:37:00.000Z",
				"review_date": "2018-07-26T15:37:00.000Z",
				"eval": "6",
				"substationtype": "1",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-26T15:37:00.000Z",
				"placement": "ACHB",
				"scada_comm_medium": "0"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

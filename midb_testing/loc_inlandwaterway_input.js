{
	"features": [{
			"jobId": "1110",
			"geometry": {
				"coordinates": [7.707752727291194, 128.82171687638697],
				"type": "Point"
			},
			"id": "inlandwaterway_facility.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"depth_max": "25",
				"oper_status": "U",
				
				"release_mark": "BW",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-25T14:44:00.000Z",
				"cc": "KS",
				"codeword": "0",
				"role_type": "LOCL",
				"depth_um": "M",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "inlandwaterway_facility",
				"depth_eval": "8",
				"fac_name": "NAEMAEL-GIL MLTEST1",
				"condition": "SLW",
				"datetime_last_chg": "2018-07-25T14:44:00.000Z",
				"review_date": "2018-07-25T14:44:00.000Z",
				"depth": "50",
				"eval": "6",
				"ff_length_um": "M",
				"category": "47100",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

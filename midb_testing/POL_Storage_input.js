{
	"features": [{
			"jobId": "1125",
			"geometry": {
				"coordinates": [47.49274670832605, 30.392911219495755],
				"type": "Point"
			},
			"id": "storage.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "POL",
				"oper_status": "U",
				"storagetype": "1",
				"release_mark": "BW",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-27T12:34:00.000Z",
				"cc": "IZ",
				"codeword": "0",
				"role_type": "LOCL",
				"class_rating": "U",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "storage",
				"fac_name": "Ramaila POL Storage MLTST3",
				"condition": "Z",
				"datetime_last_chg": "2018-07-27T12:34:00.000Z",
				"review_date": "2018-07-27T12:34:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "21700",
				"ff_length_eval": "6",
				"relative_ranking": "500",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

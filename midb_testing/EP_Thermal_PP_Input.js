{
	"features": [{
			"jobId": "1120",
			"geometry": {
				"coordinates": [37.32304105688421, 105.59645428162844],
				"type": "Point"
			},
			"id": "thermalpowerplants.undefined",
			"type": "Feature",
			"properties": {
				"electric_power_control_center": "N/A",
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"discipline": "EP",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-27T11:02:00.000Z",
				"scada_comm_medium": "0",
				"scada_protocol": "N/A",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"control_mark": "IM",
				"feature_type": "thermalpowerplants",
				"fac_name": "SHI SAOBIAN TPP MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-07-27T11:02:00.000Z",
				"review_date": "2018-07-27T11:02:00.000Z",
				"eval": "6",
				"placement": "ACHB",
				"tpptype": "1",
				"category": "42100"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

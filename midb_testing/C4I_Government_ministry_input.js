{
	"features": [{
			"jobId": "1128",
			"geometry": {
				"coordinates": [44.01279120354549, 32.597414665629984],
				"type": "Point"
			},
			"id": "government.undefined",
			"type": "Feature",
			"properties": {
				"cc": "IZ",
				"governmenttype": "2",
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "LOCL",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "C4I",
				"oper_status": "U",
				"control_mark": "IM",
				"feature_type": "government",
				"fac_name": "Karbala Min of Sports MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-07-27T14:10:00.000Z",
				"release_mark": "BW",
				"review_date": "2018-07-27T14:10:00.000Z",
				"eval": "6",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-27T14:10:00.000Z",
				"category": "77400"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

{
	"features": [{
			"jobId": "1170",
			"geometry": {
				"coordinates": [114.32424893406176, 30.45302872706605],
				"type": "Point"
			},
			"id": "dikes.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "WATER",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-01T09:01:00.000Z",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"ff_width_eval": "1",
				"control_mark": "IM",
				"feature_type": "dikes",
				"fac_name": "Wuhan Dike MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-01T09:01:00.000Z",
				"review_date": "2018-08-01T09:01:00.000Z",
				"eval": "1",
				"ff_length_um": "M",
				"category": "43800",
				"ff_length_eval": "1",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

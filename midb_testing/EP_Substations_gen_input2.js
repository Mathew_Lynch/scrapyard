{
	"features": [{
			"jobId": "1117",
			"geometry": {
				"coordinates": [22.802093266344965, 108.30638836635295],
				"type": "Point"
			},
			"id": "substations.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"discipline": "EP",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-27T09:39:00.000Z",
				"scada_comm_medium": "0",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"control_mark": "IM",
				"feature_type": "substations",
				"fac_name": "Guanxi Substation General MLTST2",
				"condition": "Z",
				"datetime_last_chg": "2018-07-27T09:39:00.000Z",
				"review_date": "2018-07-27T9:39:00.000Z",
				"eval": "6",
				"substationtype": "1",
				"placement": "ACHB",
				"category": "42600"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

{
	"features": [{
			"jobId": "1171",
			"geometry": {
				"coordinates": [114.1722312000539, 23.165915198756046],
				"type": "Point"
			},
			"id": "waterpumpstation.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "WATER",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-02T08:49:00.000Z",
				"cc": "CH",
				"codeword": "0",
				"role_type": "U",
				"class_lvl": "U",
				"ff_width_eval": "1",
				"control_mark": "IM",
				"feature_type": "waterpumpstation",
				"fac_name": "Huizhou Pumped Storage Power Stn MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-02T08:49:00.000Z",
				"review_date": "2018-08-02T08:49:00.000Z",
				"eval": "1",
				"ff_length_um": "M",
				"category": "43130",
				"ff_length_eval": "1",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

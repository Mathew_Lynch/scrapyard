{
	"features": [{
			"jobId": "1159",
			"geometry": {
				"coordinates": [114.26534556783736, 23.186954562949982],
				"type": "Point"
			},
			"id": "urbangassupply.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "POL",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-01T09:01:00.000Z",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"class_rating": "-",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "urbangassupply",
				"fac_name": "CN UGS MLTST4",
				"condition": "U",
				"datetime_last_chg": "2018-08-01T09:01:00.000Z",
				"review_date": "2018-08-01T09:01:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "43500",
				"ff_length_eval": "6",
				"relative_ranking": "500",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

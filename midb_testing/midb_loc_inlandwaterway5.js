{
	"features": [{
			"jobId": "1066",
			"geometry": {
				"coordinates": [128.9062175386842, 37.79108862131906],
				"type": "Point"
			},
			"id": "inlandwaterway_facility.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"depth_max": "120",
				"oper_status": "OPR",
				"release_mark": "BW",
				"coord": "128.9050000, 37.7913888",
				"datetime_created": "2018-07-24T09:56:00.000Z",
				"cc": "KS",
				"codeword": "0",
				"role_type": "LOCL",
				"depth_um": "M",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "inlandwaterway_facility",
				"depth_eval": "6",
				"fac_name": "Heogyun Canal MLTEST5",
				"condition": "Z",
				"datetime_last_chg": "2018-07-24T09:56:00.000Z",
				"review_date": "2018-07-24T09:56:00.000Z",
				"depth": "125",
				"eval": "6",
				"ff_length_um": "M",
				"category": "47100",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

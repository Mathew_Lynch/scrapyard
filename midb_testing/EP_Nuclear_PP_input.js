{
	"features": [{
			"jobId": "1140",
			"geometry": {
				"coordinates": [112.98975745538345, 21.925156634944926],
				"type": "Point"
			},
			"id": "nuclearpowerplants.undefined",
			"type": "Feature",
			"properties": {
				"cc": "CH",
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "NATN",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "EP",
				"oper_status": "U",
				"control_mark": "IM",
				"feature_type": "nuclearpowerplants",
				"fac_name": "Taishan NPP MLTST1",
				"condition": "UNC",
				"release_mark": "BW",
				"datetime_last_chg": "2018-07-31T09:01:00.000Z",
				"review_date": "2018-07-31T09:01:00.000Z",
				"eval": "1",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-07-31T09:01:00.000Z",
				"placement": "ACHB",
				"category": "42140",
				"scada_comm_medium": "0"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

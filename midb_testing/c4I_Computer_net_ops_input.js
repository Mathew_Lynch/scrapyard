{
	"features": [{
			"jobId": "1126",
			"geometry": {
				"coordinates": [43.02971124922085, 36.360418064236214],
				"type": "Point"
			},
			"id": "computer_network_operations.undefined",
			"type": "Feature",
			"properties": {
				"cc": "IZ",
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "LOCL",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "C4I",
				"oper_status": "U",
				"control_mark": "IM",
				"feature_type": "computer_network_operations",
				"fac_name": "Syria Rd 4c MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-07-27T13:02:00.000Z",
				"review_date": "2018-07-27T13:02:00.000Z",
				"eval": "6",
				"last_chg_userid": "osgi",
				"datetime_created": "2018-07-27T13:02:00.000Z",
				"category": "83500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

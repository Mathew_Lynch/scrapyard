{
	"features": [{
			"jobId": "1142",
			"geometry": {
				"coordinates": [122.98190128040375, 24.439872691500838],
				"type": "Point"
			},
			"id": "taps.undefined",
			"type": "Feature",
			"properties": {
				"cc": "CH",
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "LOCL",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "EP",
				"oper_status": "U",
				"control_mark": "IM",
				"feature_type": "taps",
				"fac_name": "CN TAPS MLTST1",
				"condition": "U",
				"release_mark": "BW",
				"datetime_last_chg": "2018-07-31T09:01:00.000Z",
				"review_date": "2018-07-31T09:01:00.000Z",
				"eval": "6",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-07-31T09:01:00.000Z",
				"placement": "ACHB",
				"category": "42660",
				"scada_comm_medium": "0"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

{
	"features": [{
			"jobId": "1152",
			"geometry": {
				"coordinates": [113.58733786711953, 24.69806356373473],
				"type": "Point"
			},
			"id": "controlcenters.undefined",
			"type": "Feature",
			"properties": {
				"cc": "CH",
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"codeword": "0",
				"role_type": "NATN",
				"record_status": "A",
				"domain_lvl": "CO",
				"class_lvl": "U",
				"discipline": "EP",
				"oper_status": "OPR",
				"control_mark": "IM",
				"feature_type": "controlcenters",
				"fac_name": "XSCC CC MLTST4",
				"condition": "U",
				"release_mark": "BW",
				"review_date": "2018-07-31T09:01:00.000Z",
				"datetime_last_chg": "2018-07-31T09:01:00.000Z",
				"eval": "6",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-07-31T09:01:00.000Z",
				"placement": "ACHB",
				"category": "42700",
				"scada_comm_medium": "0"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

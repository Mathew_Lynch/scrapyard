{
	"features": [{
			"jobId": "1185",
			"geometry": {
				"coordinates": [126.56127317692274, 37.41666760278629],
				"type": "Point"
			},
			"id": "bridges.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"ff_height_eval": "6",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"oper_status": "OPR",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-02T08:49:00.000Z",
				"ff_height_um": "M",
				"span_length_um": "M",
				"cc": "KN",
				"vertical_clearance_eval": "6",
				"span_qty": "2",
				"codeword": "0",
				"role_type": "REGN",
				"bridgetype": "1",
				"vertical_clearance_um": "M",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "bridges",
				"fac_name": "Incheondaegyo Bridge MLTST1",
				"condition": "RDY",
				"datetime_last_chg": "2018-08-02T08:49:00.000Z",
				"review_date": "2018-08-02T08:49:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"line_qty": "8",
				"category": "44100",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

{
	"features": [{
			"jobId": "1163",
			"geometry": {
				"coordinates": [105.33745278734246, 30.098259033230764],
				"type": "Point"
			},
			"id": "well.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "B",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "POL",
				"oper_status": "OPR",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-01T09:01:00.000Z",
				"cc": "CH",
				"codeword": "0",
				"role_type": "LOCL",
				"class_lvl": "U",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "well",
				"fac_name": "Anyue Well MLTST1",
				"condition": "UK",
				"datetime_last_chg": "2018-08-01T09:01:00.000Z",
				"review_date": "2018-08-01T09:01:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "99999",
				"ff_length_eval": "6",
				"relative_ranking": "500",
				"welltype": "2",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

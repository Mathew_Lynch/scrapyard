{
	"features": [{
			"jobId": "1188",
			"geometry": {
				"coordinates": [127.98491661071738, 38.621898791123044],
				"type": "Point"
			},
			"id": "depots.undefined",
			"type": "Feature",
			"properties": {
				"coord_deriv": "AG",
				"prod_lvl_cap": "S",
				"activity": "AAC",
				"record_status": "A",
				"domain_lvl": "CO",
				"ff_width": "500",
				"ff_width_um": "M",
				"discipline": "LOC",
				"oper_status": "U",
				"release_mark": "BW",
				"last_chg_userid": "bigbear2",
				"datetime_created": "2018-08-02T08:49:00.000Z",
				"cc": "KN",
				"depottype": "2",
				"codeword": "0",
				"role_type": "REGN",
				"class_lvl": "U",
				"funct_primary": "MTCL",
				"ff_width_eval": "6",
				"control_mark": "IM",
				"feature_type": "depots",
				"fac_name": "Kosa Depot MLTST1",
				"condition": "U",
				"datetime_last_chg": "2018-08-02T08:49:00.000Z",
				"review_date": "2018-08-02T08:49:00.000Z",
				"eval": "6",
				"ff_length_um": "M",
				"category": "92010",
				"ff_length_eval": "6",
				"ff_length": "500"
			},
			"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"
		}
	],
	"type": "FeatureCollection"
}

#Todo 
#
#import ipdb
import datetime
import random
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

##############################
#Tell FF which profile to use#
##############################

#FF_profile = webdriver.FirefoxProfile('C:/Users/Mlynch/AppData/Roaming/Mozilla/Firefox/Profiles/om5v57j2.selentstr') # Firefox
driver = webdriver.Chrome('C:/Users/Mlynch/Downloads/chromedriver_win32/chromedriver') #chrome
########################################
#Set the current date input dynamically#
########################################

def format_time():
    t = datetime.datetime.now()
    s = t.strftime('%Y-%m-%dT%H:%M:%S.%f')
    u = s[:-3]
    return str("{0}Z".format(u))

midbTime = format_time()

###########################################
#Pick random numbers to append to Fac_Name#
###########################################

def name_append():
    return str(random.randint(1,10000)) 

jeanluc = name_append()
picard = name_append()


##################
#Begin unit tests#
##################

class FISHNetToMIDB_C4I_RadioComm(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        #self.browser = webdriver.Firefox(FF_profile) #FireFox
        self.browser = driver #Chrome
        
    def test_a_login(self):  #Login to FISHNet
        browser = self.browser
        browser.get('https://geoserver.fishnet-project.io/acknowledge')
        #browser.set_window_position(2000,0)
        browser.maximize_window()
        #ipdb.set_trace()
        #browser.implicitly_wait(8) #Need to pause the webdriver for the page to fully load otherwis element not found
        button = browser.find_element_by_xpath('//form[contains(@class, "ui form")]/button[contains(@type, "submit")]')
        button.click()
        button2 = browser.find_element_by_xpath('//div[contains(@class, "modal-actions cf")]/button[contains(@class, "start-editing col6")]')
        button2.click()
        browser.get('https://geoserver.fishnet-project.io/#background=OSM%20Basemap&map=16.00/23.1082/113.3191')
        
    def test_b_search_for_title(self):  #Small test can keep or throw away
        browser = self.browser
        self.assertIn("FISHNet", browser.title)

    def test_c_select_map_data_Disc(self):  #Will need to break out each discipline to test in their own set of cases
        browser = self.browser
        #wait = WebDriverWait(self.browser, 10) #Needed to add this wait in this mannor as the page is not loading consistantly.
        button = browser.find_element_by_xpath('//div[contains(@class, "map-control map-data-control")]/button')
        button.click()
        time.sleep(1)
        #wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/div/div[2]/div[1]/div[5]/div[3]/div/div[2]/div[1]/div/ul/li[4]/label/span")))       
        buttonDIS = browser.find_element_by_xpath('//div[contains(@class, "map-data-disciplines")]//div[contains(@class, "disclosure-wrap disclosure-wrap-data_disciplines")]/ul[contains(@class, "layer-list layer-discipline-list")]/li[2]/label/span')
        buttonDIS.click()
        time.sleep(1)
        closeMDmenue = browser.find_element_by_xpath('//div[contains(@class, "map-control map-data-control")]/button').click()
        
#    def test_d_zoomIn(self):  #Zoom in the map
#        browser = self.browser
#        action = ActionChains(browser)
#        zoomIn = browser.find_element_by_xpath('//div[contains(@class, "map-control zoombuttons")]/button[contains(@class, "zoom-in")]')
#        for i in range(2): #Note - 12 is minimum to get to place a point
#            zoomIn.click()
#            time.sleep(.8)
        
#    def test_e_scrollMap(self): #Scroll map
#        browser = self.browser
#        action = ActionChains(browser)
#        zoomIn = browser.find_element_by_xpath('//div[contains(@class, "map-control zoombuttons")]/button[contains(@class, "zoom-in")]')
#        clickMap = browser.find_element_by_xpath('//*[@id="surface"]')
#        for i in range(2):
#            action.drag_and_drop_by_offset(clickMap, 168, 1215).perform()
#            time.sleep(1.5)
#        for i in range(9): #Note - 11 is minimum to get to place a point
#            zoomIn.click()
#            time.sleep(.9)

#    def test_f_centerPoint(self): #Bring Marker into view - was getting outofbounds errors on viewport so this is a workaround
#        browser = self.browser
#        action = ActionChains(browser)
#        button = browser.find_element_by_xpath('//div[contains(@class, "button-wrap joined col3")]/button')
#        button.click()
#        time.sleep(2)
#        button.click()
#        zoomIn = browser.find_element_by_xpath('//div[contains(@class, "map-control zoombuttons")]/button[contains(@class, "zoom-in")]')
#        for i in range(3):
#            action.send_keys(Keys.LEFT).perform()
#            time.sleep(.4)
#        for i in range(2):
#            action.send_keys(Keys.UP).perform()
#            time.sleep(.4)
#        for i in range(2):  #Final Zoom
#            zoomIn.click()
#            time.sleep(1)
#        for i in range(3):
 #           action.send_keys(Keys.LEFT).perform()
  #          time.sleep(2)
   #     for i in range(9):
    #        action.send_keys(Keys.UP).perform()
     #       time.sleep(2)

    def test_g_markPoint(self):  #Place Marker Point
        browser = self.browser
        button = browser.find_element_by_xpath('//div[contains(@class, "button-wrap joined col3")]//button[1]')
        button.click()
        action = ActionChains(browser)
        x = random.randint(-60,60)
        xoffset = x + 550
        y = random.randint(-60,60)
        yoffset = y + 384
        clickMap = browser.find_element_by_xpath('//div[contains(@class, "layer layer-imagery")]')
        #action.move_to_element_with_offset(clickMap, 384, 880).click().perform() #Center point of map #Full screen Portrait
        #action.move_to_element_with_offset(clickMap, 550, 384).click().perform() #Center point of map #Full screen landscape laptop
        action.move_to_element_with_offset(clickMap, xoffset, yoffset).click().perform() #Custom point center
        time.sleep(2)
        
    def test_h_Select_FeatureType(self):  #Edit the features about the point
        browser = self.browser
        action = ActionChains(browser)
        #featureType = browser.find_element_by_xpath('//div[contains(@class, "preset-list-pane pane")]')
        ###waitlelement = wait.until(EC.invisibility_of_element_located((By.XPATH, '//div/contains(@class, "preset-list-item preset-LOC-Airfield")]')))
        copa = browser.find_element_by_css_selector('#sidebar > div.inspector-wrap.fr > div.panewrap > div.preset-list-pane.pane > div.inspector-body > div > div.preset-list-item.preset-category-Radio_Comm > div.preset-list-button-wrap.category.col12 > button')
        copa.click()
        time.sleep(1)
        for i in range(5):
            action.send_keys(Keys.TAB).perform()
        #copb = browser.find_element_by_css_selector('#sidebar > div.inspector-wrap.fr > div.panewrap > div.preset-list-pane.pane > div.inspector-body > div > div.preset-list-item.preset-category-Radio_Comm > div.subgrid.col12 > div.preset-list.fillL3.cf.fl > div.preset-list-item.preset-C4I-Radio_Comm\2f TroposphericStn > div.preset-list-button-wrap.col12 > button')
        copb = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div[1]/div[1]/div[2]/div/div[6]/div[2]/div[2]/div[7]/div[1]/button')
        copb.click()
        time.sleep(1)
            
    def test_i_Select_CountryCode(self): 
        browser = self.browser
        cca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CC_0")]//div[contains(@class, "combobox-caret")]')
        cca.click()
        cc1 = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "China")]')
        cc1.click()
        
    def test_j_Select_classlvl(self):
        browser = self.browser
        clla = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CLASS_LVL modified present")]//div[contains(@class, "combobox-caret")]')
        clla.click()
        cllb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unclassified")]')
        cllb.click()
                       
    def test_k_Select_Phys_Con(self):
        browser = self.browser
        physCona = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CONDITION_2 modified present")]//div[contains(@class, "combobox-caret")]')
        physCona.click()
        physConb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Target Status Unknown; CPD is Greater Than 0% and Less Than 10%")]')
        physConb.click()
                
    def test_l_Select_DateTime_C(self):
        browser = self.browser
        action = ActionChains(browser)
        dtca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-DATETIME_CREATED")]')
        dtca.click()
        dtc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME_CREATED"]')
        dtc1.click()
        action.send_keys(midbTime).perform()
        time.sleep(1)
        
    def test_m_Select_DateTime_LC(self):
        browser = self.browser
        action = ActionChains(browser)
        dtlca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-DATETIME_LAST_CHG")]')
        dtlca.click()
        dtlc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME_LAST_CHG"]')
        dtlc1.click()
        action.send_keys(midbTime).perform()
        time.sleep(1)


    def test_n_Select_Confidence(self):
        browser = self.browser
        #confa = browser.find_element_by_xpath('//div[contains(@class, "wrap-form-field wrap-form-field-EVAL_3 modified present")]//div[contains(@class, "combobox-caret")]')
        confa = browser.find_element_by_css_selector('#sidebar > div.inspector-wrap.fr > div.panewrap > div.entity-editor-pane.pane > div.inspector-body > div.inspector-border.preset-editor > div > div > div.wrap-form-field.wrap-form-field-EVAL_1 > div > div.combobox-caret')
        confa.click()
        time.sleep(2)
        #confb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "TRUTH CANNOT BE JUDGED. Investigation of subject matter reveals no basis for allocating any of the ratings confirmed thru improbably. To avoid the inaccurate use of the other ratings use this rating when no information is available for comparison.")]')
        confb = browser.find_element_by_css_selector('#id-container > div.combobox > a:nth-child(6)')
        confb.click()

    def test_o_Select_Facility_Name(self):
        browser = self.browser
        action = ActionChains(browser)
        #fac_namea = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-FAC_NAME modified present")]')
        fac_namea = browser.find_element_by_css_selector('#sidebar > div.inspector-wrap.fr > div.panewrap > div.entity-editor-pane.pane > div.inspector-body > div.inspector-border.preset-editor > div > div > div.wrap-form-field.wrap-form-field-FAC_NAME > div > label > div')
        fac_namea.click()
        #fac_nameb = browser.find_element_by_xpath('//*[@id="preset-input-FAC_NAME"]')
        fac_nameb = browser.find_element_by_css_selector('#preset-input-FAC_NAME')
        fac_nameb.click()
        for i in range(3):
            action.send_keys(Keys.BACKSPACE).perform()
        time.sleep(1)
        action.send_keys("CN Radio Comm A{0}T{1}".format(jeanluc,picard)).perform()
        time.sleep(2)

    def test_ma_Select_HIOW(self):
        browser = self.browser   
        #emsl = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-ELEVATION_MSL_0 modified present")]//div[contains(@class, "spin-control")]//button[contains(@class, "increment")]')
        hiow = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div[1]/div[2]/div[2]/div[3]/div/div/div[17]/div/div[1]/button[2]')
        time.sleep(1)
        hiow.click()

    def test_oa_Select_FHE(self):
        browser = self.browser   
        fhe = browser.find_element_by_xpath('//div[contains(@class, "form-field-FF_HEIGHT_EVAL_1 modified present")]//button[contains(@title, "remove")]')
        fhe.click()
        time.sleep(1)

    def test_ob_Select_HUM(self):
        browser = self.browser   
        hum = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-FF_HEIGHT_UM modified present")]//button[contains(@title, "remove")]')
        hum.click()
        time.sleep(1)
        
    def test_v_Select_LCUID(self):
        browser = self.browser
        action = ActionChains(browser)        
        lcuid = browser.find_element_by_xpath('//*[@id="preset-input-LAST_CHG_USERID"]')
        lcuid.click()
        time.sleep(2)
        action.send_keys("bigbear2").perform()
        time.sleep(2)

    def test_x_Select_OS(self):
        browser = self.browser
        osa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-OPER_STATUS_1 modified present")]//div[contains(@class, "combobox-caret")]')
        osa.click()
        osb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unknown")]')
        osb.click()

    def test_y_Select_PLCOP(self):
        browser = self.browser
        plcopa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-PROD_LVL_CAP_2 modified present")]//div[contains(@class, "combobox-caret")]')
        plcopa.click()
        plcopb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "BASIC")]')
        plcopb.click()

    def test_z_Select_RS(self):
        browser = self.browser
        rsa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECORD_STATUS modified present")]//div[contains(@class, "combobox-caret")]')
        rsa.click()
        rsb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Active")]')
        rsb.click()

    def test_za_Select_BCT(self):
        browser = self.browser
        bcta = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_BKUP_CAP_TIME modified present")]//button[contains(@title, "remove")]')
        bcta.click()

    def test_zb_Select_BCTUM(self):
        browser = self.browser
        bctuma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_BKUP_CAP_TIME_UOM modified present")]//button[contains(@title, "remove")]')
        bctuma.click()

    def test_zc_Select_RIMin(self):
        browser = self.browser
        rimna = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL modified present")]//button[contains(@title, "remove")]')
        rimna.click()

    def test_zd_Select_RIMax(self):
        browser = self.browser
        rimxa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL_MAX modified present")]//button[contains(@title, "remove")]')
        rimxa.click()

    def test_ze_Select_RIUM(self):
        browser = self.browser
        riuma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL_UM_1 modified present")]//button[contains(@title, "remove")]')
        riuma.click()

    def test_zf_Select_RLI(self):
        browser = self.browser
        rlia = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_LOSS_IMPACT modified present")]//button[contains(@title, "remove")]')
        rlia.click()

    def test_zg_Select_RRP(self):
        browser = self.browser
        rrpa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_REPAIR_PRIORITY_1 modified present")]//button[contains(@title, "remove")]')
        rrpa.click()

    def test_zh_Select_RM(self):
        browser = self.browser
        rma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RELEASE_MARK modified present")]')
        rma.click()

    def test_zi_Select_ReviewDate(self):
        browser = self.browser
        action = ActionChains(browser)
        rda = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-REVIEW_DATE")]')
        rda.click()
        rd1 = browser.find_element_by_xpath('//*[@id="preset-input-REVIEW_DATE"]')
        rd1.click()
        action.send_keys(midbTime).perform()
        time.sleep(2)

    def test_zj_Select_RoleType(self):
        browser = self.browser
        rta = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-ROLE_TYPE_1 modified present")]//div[contains(@class, "combobox-caret")]')
        rta.click()
        rtb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unknown.")]')
        rtb.click()

    def test_zl_Select_Save(self): #Save and Upload
        browser = self.browser
        sva = browser.find_element_by_xpath('//div[contains(@class, "button-wrap col1")]/button[contains(@class, "save col12 has-count")]')
        sva.click()
        time.sleep(1)
        svb = browser.find_element_by_xpath('//div[contains(@class, "buttons fillL cf")]/button[contains(@class, "action col5 button save-button")]')
        svb.click()
        time.sleep(2)
        
###############
#Close Browser#
###############
        
    def test_zm_tearDown(self):
        browser = self.browser
        wait = WebDriverWait(browser, 8) #Note this tells the browser to wait UPTO, not fixed
        waitelement = wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@class, "ui large secondary pointing menu")]//a[contains(@class, "item active")]')))
        self.browser.close()
        
        
###############
#Main Function#
###############

if __name__ == "__main__":
    unittest.main(verbosity=2)

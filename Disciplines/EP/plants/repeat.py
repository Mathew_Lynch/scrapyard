def add_points(self):
        test_g_markPoint(self)
        test_h_Select_FeatureType(self)
        test_ha_Select_Capacity(self)
        test_hb_Select_CapEval(self)
        test_ha_Select_CapEvalUM(self)
        test_i_Select_CountryCode(self)
        test_j_Select_classlvl(self)
        test_k_Select_Phys_Con(self)
        test_l_Select_DateTime_C(self)
        test_m_Select_DateTime_LC(self)
        test_ma_Select_EPCC(self)
	test_n_Select_Confidence(self)
        test_o_Select_Facility_Name(self)
        test_v_Select_LCUID(self)
        test_x_Select_OS(self)
        test_y_Select_PLCOP(self)
        test_z_Select_RS(self)
        test_za_Select_BCT(self)
        test_zb_Select_BCTUM(self)
        test_zc_Select_RIMin(self)
        test_zd_Select_RIMax(self)
        test_ze_Select_RIUM(self)
        test_zf_Select_RLI(self)
        test_zg_Select_RRP(self)
        test_zh_Select_RM(self)
        test_zi_Select_ReviewDate(self)
        test_zj_Select_RoleType(self)
        test_zk_Select_SCADA(self)
	

##########################################################
def test_g_markPoint(self):  #Place Marker Point
        browser = self.browser
        button = browser.find_element_by_xpath('//div[contains(@class, "button-wrap joined col3")]//button[1]')
        button.click()
        action = ActionChains(browser)
        x = random.randint(-60,60)
        xoffset = x + 520
        y = random.randint(-60,60)
        yoffset = y + 384
        clickMap = browser.find_element_by_xpath('//div[contains(@class, "layer layer-imagery")]')
        #action.move_to_element_with_offset(clickMap, 384, 880).click().perform() #Center point of map #Full screen Portrait
        #action.move_to_element_with_offset(clickMap, 550, 384).click().perform() #Center point of map #Full screen landscape laptop
        action.move_to_element_with_offset(clickMap, xoffset, yoffset).click().perform() #Custom point center
        time.sleep(2)
            
def test_h_Select_FeatureType(self):  #Edit the features about the point
	browser = self.browser
	#action = ActionChains(browser)
	#featureType = browser.find_element_by_xpath('//div[contains(@class, "preset-list-pane pane")]')
	###waitlelement = wait.until(EC.invisibility_of_element_located((By.XPATH, '//div/contains(@class, "preset-list-item preset-LOC-Airfield")]')))
	#yards = browser.find_element_by_css_selector(".preset-EP-NuclearPowerPlants > div:nth-child(1) > button:nth-child(1)")
	npp = browser.find_element_by_xpath("/html/body/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[6]/div[1]/button")
	npp.click()
	time.sleep(2)
	#railroadYard = browser.find_element_by_xpath('//div[contains(@class, "preset-list-item preset-LOC-Yards/Railroad_Yard")]/div/button')
	#railroadYard.click()

def test_ha_Select_Capacity(self):
	browser = self.browser
	cap = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CAPACITY_1 modified present")]//button[contains(@title, "remove")]')
	cap.click()

def test_hb_Select_CapEval(self):
	browser = self.browser
	cape = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CAPACITY_EVAL modified present")]//button[contains(@title, "remove")]')
	cape.click()

def test_ha_Select_CapEvalUM(self):
	browser = self.browser
	capeum = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CAPACITY_UM_0 modified present")]//button[contains(@title, "remove")]')
	capeum.click()
	
def test_i_Select_CountryCode(self): 
	browser = self.browser
	clla = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CLASS_LVL modified present")]') #Bring into view
	clla.click()
	cdw = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CODEWORD modified present")]')
	cdw.click()
	cca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CC_0")]//div[contains(@class, "combobox-caret")]')
	cca.click()
	cc1 = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "China")]')
	cc1.click()
	
def test_j_Select_classlvl(self):
	browser = self.browser
	cdw = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CODEWORD modified present")]')
	cdw.click()
	physCona = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CONDITION_2 modified present")]')
	physCona.click()
	clla = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CLASS_LVL modified present")]//div[contains(@class, "combobox-caret")]')
	clla.click()
	cllb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unclassified")]')
	cllb.click()
				   
def test_k_Select_Phys_Con(self):
	browser = self.browser
	physCona = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CONDITION_2 modified present")]//div[contains(@class, "combobox-caret")]')
	physCona.click()
	physConb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Target Status Unknown; CPD is Greater Than 0% and Less Than 10%")]')
	physConb.click()
			
def test_l_Select_DateTime_C(self):
	browser = self.browser
	action = ActionChains(browser)
	dtca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-DATETIME_CREATED")]')
	dtca.click()
	dtc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME_CREATED"]')
	dtc1.click()
	action.send_keys(midbTime).perform()
	time.sleep(1)
	
def test_m_Select_DateTime_LC(self):
	browser = self.browser
	action = ActionChains(browser)
	dtlca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-DATETIME_LAST_CHG")]')
	dtlca.click()
	dtlc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME_LAST_CHG"]')
	dtlc1.click()
	action.send_keys(midbTime).perform()
	time.sleep(1)

def test_ma_Select_EPCC(self):
	browser = self.browser   
	epcc = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-ELECTRIC_POWER_CONTROL_CENTER modified present")]//button[contains(@title, "remove")]')
	epcc.click()
	time.sleep(1)


def test_n_Select_Confidence(self):
	browser = self.browser
	#confa = browser.find_element_by_xpath('//div[contains(@class, "wrap-form-field wrap-form-field-EVAL_3 modified present")]//div[contains(@class, "combobox-caret")]')
	confa = browser.find_element_by_css_selector('.form-field-EVAL_3 > div:nth-child(3)')
	confa.click()
	time.sleep(2)
	#confb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "TRUTH CANNOT BE JUDGED. Investigation of subject matter reveals no basis for allocating any of the ratings confirmed thru improbably. To avoid the inaccurate use of the other ratings use this rating when no information is available for comparison.")]')
	confb = browser.find_element_by_css_selector('#id-container > div.combobox > a:nth-child(1)')
	confb.click()

def test_o_Select_Facility_Name(self):
	browser = self.browser
	action = ActionChains(browser)
	#fac_namea = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-FAC_NAME modified present")]')
	fac_namea = browser.find_element_by_css_selector('.form-field-FAC_NAME > label:nth-child(1) > div:nth-child(1)')
	fac_namea.click()
	#fac_nameb = browser.find_element_by_xpath('//*[@id="preset-input-FAC_NAME"]')
	fac_nameb = browser.find_element_by_css_selector('#preset-input-FAC_NAME')
	fac_nameb.click()
	for i in range(3):
		action.send_keys(Keys.BACKSPACE).perform()
	action.send_keys("CN NPP A{0}T{1}".format(jeanluc,picard)).perform()

def test_v_Select_LCUID(self):
	browser = self.browser
	action = ActionChains(browser)        
	lcuid = browser.find_element_by_xpath('//*[@id="preset-input-LAST_CHG_USERID"]')
	lcuid.click()
	time.sleep(2)
	action.send_keys("bigbear2").perform()
	time.sleep(2)

def test_x_Select_OS(self):
	browser = self.browser
	osa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-OPER_STATUS_1 modified present")]//div[contains(@class, "combobox-caret")]')
	osa.click()
	osb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unknown")]')
	osb.click()

def test_y_Select_PLCOP(self):
	browser = self.browser
	plcopa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-PROD_LVL_CAP_1 modified present")]//div[contains(@class, "combobox-caret")]')
	plcopa.click()
	plcopb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "BASIC")]')
	plcopb.click()

def test_z_Select_RS(self):
	browser = self.browser
	rsa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECORD_STATUS modified present")]//div[contains(@class, "combobox-caret")]')
	rsa.click()
	rsb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Active")]')
	rsb.click()

def test_za_Select_BCT(self):
	browser = self.browser
	bcta = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_BKUP_CAP_TIME modified present")]//button[contains(@title, "remove")]')
	bcta.click()

def test_zb_Select_BCTUM(self):
	browser = self.browser
	bctuma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_BKUP_CAP_TIME_UOM modified present")]//button[contains(@title, "remove")]')
	bctuma.click()

def test_zc_Select_RIMin(self):
	browser = self.browser
	rimna = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL modified present")]//button[contains(@title, "remove")]')
	rimna.click()

def test_zd_Select_RIMax(self):
	browser = self.browser
	rimxa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL_MAX modified present")]//button[contains(@title, "remove")]')
	rimxa.click()

def test_ze_Select_RIUM(self):
	browser = self.browser
	riuma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL_UM_1 modified present")]//button[contains(@title, "remove")]')
	riuma.click()

def test_zf_Select_RLI(self):
	browser = self.browser
	rlia = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_LOSS_IMPACT modified present")]//button[contains(@title, "remove")]')
	rlia.click()

def test_zg_Select_RRP(self):
	browser = self.browser
	rrpa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_REPAIR_PRIORITY_1 modified present")]//button[contains(@title, "remove")]')
	rrpa.click()

def test_zh_Select_RM(self):
	browser = self.browser
	rma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RELEASE_MARK modified present")]')
	rma.click()

def test_zi_Select_ReviewDate(self):
	browser = self.browser
	action = ActionChains(browser)
	rda = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-REVIEW_DATE")]')
	rda.click()
	rd1 = browser.find_element_by_xpath('//*[@id="preset-input-REVIEW_DATE"]')
	rd1.click()
	action.send_keys(midbTime).perform()
	time.sleep(2)

def test_zj_Select_RoleType(self):
	browser = self.browser
	rta = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-ROLE_TYPE_1 modified present")]//div[contains(@class, "combobox-caret")]')
	rta.click()
	rtb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unknown.")]')
	rtb.click()

def test_zk_Select_SCADA(self):
	browser = self.browser
	scda = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-SCADA_COMM_MEDIUM modified present")]//div[contains(@class, "combobox-caret")]')
	scda.click()
	time.sleep(2)
	scdb = browser.find_element_by_xpath('//*[@id="id-container"]/div[1]/a[1]')
	scdb.click()

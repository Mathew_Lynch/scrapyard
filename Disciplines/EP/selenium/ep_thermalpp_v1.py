"""Todo: Incorporate other disciplines for payload testing.
"""
import datetime
import random
import time
import unittest
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.alert import Alert
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

# from repeat import add_points
# import repeat

########################################
# Tell FF which profile to use         #
# You will need to edit for your sytem #
########################################

# FF_profile = webdriver.FirefoxProfile('C:/Users/Mlynch/AppData/Roaming/\
# Mozilla/Firefox/Profiles/om5v57j2.selentstr') # Firefox


DRIVER = webdriver.Chrome('C:/Users/Mlynch/Downloads/chromedriver_win32/\
chromedriver')  # chrome


def ap_ep_npp(self):
    """Bunch of tests"""
    test_g_markpoint(self)
    test_h_select_featuretype(self)
    test_ha_select_capacity(self)
    test_hb_select_capeval(self)
    test_hc_select_capevalum(self)
    test_i_select_countrycode(self)
    test_j_select_classlvl(self)
    test_k_select_phys_con(self)
    test_l_select_datetime_c(self)
    test_m_select_datetime_lc(self)
    test_ma_select_epcc(self)
    test_n_select_confidence(self)
    test_o_select_facility_name(self)
    test_v_select_lcuid(self)
    test_x_select_os(self)
    test_y_select_plcop(self)
    test_z_select_rs(self)
    test_za_select_bct(self)
    test_zb_select_bctum(self)
    test_zc_select_rimin(self)
    test_zd_select_rimax(self)
    test_ze_select_rium(self)
    test_zf_select_rli(self)
    test_zg_select_rpp(self)
    test_zh_select_rm(self)
    test_zi_select_reviewdate(self)
    test_zj_select_roletype(self)
    test_zk_select_scada(self)
    # test_zl_select_all_tags(self)
    # test_zm_select_plcop(self)
    # test_zn_select_record_status(self)


def test_g_markpoint(self):
    """Place Marker Point"""
    browser = self.browser
    button = browser.find_element_by_xpath('//div[contains(@class, \
"button-wrap joined col3")]//button[1]')
    button.click()
    action = ActionChains(browser)
    xaxis = random.randint(-60, 60)
    # xoffset = x + 520 #First point
    xoffset = xaxis + 350
    yaxis = random.randint(-60, 60)
    # yoffset = y + 384 #First Point
    yoffset = yaxis + 334
    clickmap = browser.find_element_by_xpath('//div[contains(@class, "layer \
layer-imagery")]')
    # enter point of map #Full screen Portrait
    # action.move_to_element_with_offset(clickMap, 384, 880).click().perform()
    # Center point of map #Full screen landscape laptop
    # action.move_to_element_with_offset(clickMap, 550, 384).click().perform()
    # Custom point center
    action.move_to_element_with_offset(clickmap, xoffset,
                                       yoffset).click().perform()
    time.sleep(2)
    print('test_g_markPoint')


def test_h_select_featuretype(self):
    """Edit the features about the point"""
    browser = self.browser
    tppa = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/div[1]\
/div[2]/div/div[3]/div[1]/button/div[3]')
    tppa.click()
    time.sleep(.5)
    tppb = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/div[1]\
/div[2]/div/div[3]/div[2]/div[2]/div[1]/div[1]/button/div[3]')
    tppb.click()
    time.sleep(.5)
    print('test_h_select_FeatureTyp')


def test_ha_select_capacity(self):
    """Select_Capacity"""
    browser = self.browser
    cap = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CAPACITY_1 modified present")]//button[contains(@title, "remove")]')
    cap.click()
    print('test_ha_select_Capacity')


def test_hb_select_capeval(self):
    """Select_CapEval"""
    browser = self.browser
    cape = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CAPACITY_EVAL modified present")]//button[contains(@title, \
"remove")]')
    cape.click()
    print('test_hb_select_CapEval')


def test_hc_select_capevalum(self):
    """Select_CapEvalUM"""
    browser = self.browser
    capeum = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-CAPACITY_UM_0 modified present")]//button[contains\
(@title, "remove")]')
    capeum.click()
    print('test_hc_select_CapEvalUM')


def test_i_select_countrycode(self):
    """Select_CountryCode"""
    browser = self.browser
    clla = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CLASS_LVL modified present")]')  # Bring into view
    clla.click()
    cdw = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CODEWORD modified present")]')
    cdw.click()
    cca = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CC_0")]//div[contains(@class, "combobox-caret")]')
    cca.click()
    cc1 = browser.find_element_by_xpath('//div[contains(@class, "combobox")]\
//a[contains(@title, "China")]')
    cc1.click()
    print('test_i_select_CountryCode')


def test_j_select_classlvl(self):
    """Select_classlvl"""
    browser = self.browser
    cdw = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CODEWORD modified present")]')
    cdw.click()
    physcona = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-CONDITION_2 modified present")]')
    physcona.click()
    clla = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-CLASS_LVL modified present")]//div[contains(@class, \
"combobox-caret")]')
    clla.click()
    cllb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]\
//a[contains(@title, "Unclassified")]')
    cllb.click()
    print('test_j_select_classlvl')


def test_k_select_phys_con(self):
    """Select_Phys_Con"""
    browser = self.browser
    physcona = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-CONDITION_2 modified present")]//div[contains(@class, \
"combobox-caret")]')
    physcona.click()
    physconb = browser.find_element_by_xpath('//div[contains(@class, \
"combobox")]//a[contains(@title, "Target Status Unknown; CPD is Greater \
Than 0% and Less Than 10%")]')
    physconb.click()
    print('test_k_select_Phys_Con')


def test_l_select_datetime_c(self):
    """Select_DateTime_C"""
    tcurrent = datetime.datetime.now()
    scurrent = tcurrent.strftime('%Y-%m-%dT%H:%M:%S.%f')
    ucurrent = scurrent[:-3]
    midbtime = str("{0}Z".format(ucurrent))
    browser = self.browser
    action = ActionChains(browser)
    dtca = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-DATETIME_CREATED")]')
    dtca.click()
    dtc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME\
_CREATED"]')
    dtc1.click()
    action.send_keys(midbtime).perform()
    time.sleep(1)
    print('test_l_select_datetime_c')


def test_m_select_datetime_lc(self):
    """Select_DateTime_L"""
    tcurrent = datetime.datetime.now()
    scurrent = tcurrent.strftime('%Y-%m-%dT%H:%M:%S.%f')
    ucurrent = scurrent[:-3]
    midbtime = str("{0}Z".format(ucurrent))
    browser = self.browser
    action = ActionChains(browser)
    dtlca = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-DATETIME_LAST_CHG")]')
    dtlca.click()
    dtlc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME\
_LAST_CHG"]')
    dtlc1.click()
    action.send_keys(midbtime).perform()
    time.sleep(1)
    print('test_m_select_DateTime_LC')


def test_ma_select_epcc(self):
    """Select_EPCC"""
    browser = self.browser
    epcc = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-ELECTRIC_POWER_CONTROL_CENTER modified present")]//button[contains\
(@title, "remove")]')
    epcc.click()
    time.sleep(1)
    print('test_ma_select_EPCC')


def test_n_select_confidence(self):
    """Select_Confidence"""
    browser = self.browser
    # confa = browser.find_element_by_xpath('//div[contains(@class, \
# "wrap-form-field wrap-form-field-EVAL_3 modified present")]//div[contains\
# (@class, "combobox-caret")]')
    confa = browser.find_element_by_css_selector('.form-field-EVAL_3 > \
div:nth-child(3)')
    confa.click()
    time.sleep(2)
    # confb = browser.find_element_by_xpath('//div[contains(@class, \
# "combobox")]//a[contains(@title, "TRUTH CANNOT BE JUDGED. Investigation of \
# subject matter reveals no basis for allocating any of the ratings confirmed \
# thru improbably. To avoid the inaccurate use of the other ratings use this \
# rating when no information is available for comparison.")]')
    confb = browser.find_element_by_css_selector('#id-container > \
div.combobox > a:nth-child(1)')
    confb.click()
    print('test_n_select_Confidence')


def test_o_select_facility_name(self):
    """Select_Facility_Name"""
    jeanluc = str(random.randint(1, 10000))
    picard = str(random.randint(1, 10000))
    browser = self.browser
    action = ActionChains(browser)
    # fac_namea = browser.find_element_by_xpath('//div[contains(@class, \
# "form-field form-field-FAC_NAME modified present")]')
    fac_namea = browser.find_element_by_css_selector('.form-field-FAC_NAME > \
label:nth-child(1) > div:nth-child(1)')
    fac_namea.click()
    # fac_nameb = browser.find_element_by_xpath('//*[@id="preset-input\
# -FAC_NAME"]')
    fac_nameb = browser.find_element_by_css_selector('#preset-input-FAC_NAME')
    fac_nameb.click()
    for _i in range(3):
        action.send_keys(Keys.BACKSPACE).perform()
    action.send_keys("CN Thermal pp A{0}T{1}".format(jeanluc, picard)).perform()
    print('test_o_select_Facility_Name')


def test_v_select_lcuid(self):
    """Select_LCUID"""
    browser = self.browser
    action = ActionChains(browser)
    lcuid = browser.find_element_by_xpath('//*[@id="preset-input\
-LAST_CHG_USERID"]')
    lcuid.click()
    time.sleep(2)
    action.send_keys("bigbear2").perform()
    time.sleep(2)
    print('test_v_select_LCUID')


def test_x_select_os(self):
    """Select_OS"""
    browser = self.browser
    action = ActionChains(browser)
    osa = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-OPER_STATUS_1 modified present")]//div[contains(@class, \
"combobox-caret")]')
    osa.click()
    osb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]\
//a[contains(@title, "Unknown")]')
    osb.click()
    for _i in range(1):
        action.send_keys(Keys.PAGE_DOWN).perform()
        time.sleep(.5)
    print('test_x_select_OS')


def test_y_select_plcop(self):
    """Select_PLCOP"""
    browser = self.browser
    action = ActionChains(browser)
    plcopa = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-PROD_LVL_CAP_1 modified present")]//div[contains\
(@class, "combobox-caret")]')
    plcopa.click()
    time.sleep(1)
    action.send_keys(Keys.PAGE_DOWN).perform()
    time.sleep(.5)
    plcopb = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/\
div[2]/div[2]/div[4]/div/div/div[29]/div/div[1]')
    for _i in range(2):
        plcopb.click()
        time.sleep(1)
    plcopc = browser.find_element_by_xpath('//*[@id="id-container"]/\
div[1]/a[2]')
    plcopc.click()
    print('test_y_select_PLCOP')


def test_z_select_rs(self):
    """Select_RS"""
    browser = self.browser
    rsa = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-RECORD_STATUS modified present")]//div[contains(@class, \
"combobox-caret")]')
    rsa.click()
    rsb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]\
//a[contains(@title, "Active")]')
    rsb.click()
    print('test_z_select_RS')


def test_za_select_bct(self):
    """Select_BCT"""
    browser = self.browser
    bcta = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-RECUP_BKUP_CAP_TIME modified present")]//button[contains\
(@title, "remove")]')
    bcta.click()
    print('test_za_select_BCT')


def test_zb_select_bctum(self):
    """Select_BCTUM"""
    browser = self.browser
    bctuma = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RECUP_BKUP_CAP_TIME_UOM modified present")]\
//button[contains(@title, "remove")]')
    bctuma.click()
    print('test_zb_select_BCTUM')


def test_zc_select_rimin(self):
    """Select_RIMin"""
    browser = self.browser
    rimna = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RECUP_INTRVL modified present")]//button\
[contains(@title, "remove")]')
    rimna.click()
    print('test_zc_select_RIMin')


def test_zd_select_rimax(self):
    """Select_RIMax"""
    browser = self.browser
    rimxa = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RECUP_INTRVL_MAX modified present")]\
//button[contains(@title, "remove")]')
    rimxa.click()
    print('test_zd_select_RIMax')


def test_ze_select_rium(self):
    """Select_RIUM"""
    browser = self.browser
    riuma = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RECUP_INTRVL_UM_1 modified present")]\
//button[contains(@title, "remove")]')
    riuma.click()
    print('test_ze_select_RIUM')


def test_zf_select_rli(self):
    """Select_RLI"""
    browser = self.browser
    rlia = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RECUP_LOSS_IMPACT modified present")]\
//button[contains(@title, "remove")]')
    rlia.click()
    print('test_zf_select_RLI')


def test_zg_select_rpp(self):
    """Select_RRP"""
    browser = self.browser
    rrpa = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RECUP_REPAIR_PRIORITY_1 modified present")]\
//button[contains(@title, "remove")]')
    rrpa.click()
    print('test_zg_select_RRP')


def test_zh_select_rm(self):
    """Select_RM"""
    browser = self.browser
    rma = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-RELEASE_MARK modified present")]')
    rma.click()
    print('test_zh_select_RM')


def test_zi_select_reviewdate(self):
    """Select_ReviewDate"""
    tcurrent = datetime.datetime.now()
    scurrent = tcurrent.strftime('%Y-%m-%dT%H:%M:%S.%f')
    ucurrent = scurrent[:-3]
    midbtime = str("{0}Z".format(ucurrent))
    browser = self.browser
    action = ActionChains(browser)
    rda = browser.find_element_by_xpath('//div[contains(@class, \
"form-field form-field-REVIEW_DATE")]')
    rda.click()
    rd1 = browser.find_element_by_xpath('//*[@id="preset-input-REVIEW_DATE"]')
    rd1.click()
    action.send_keys(midbtime).perform()
    time.sleep(2)
    print('test_zi_select_ReviewDate')


def test_zj_select_roletype(self):
    """Select_RoleType"""
    browser = self.browser
    rta = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-ROLE_TYPE_1 modified present")]//div[contains(@class, \
"combobox-caret")]')
    rta.click()
    rtb = browser.find_element_by_xpath('//div[contains(@class, \
"combobox")]//a[contains(@title, "Unknown.")]')
    rtb.click()
    print('test_zj_select_RoleType')


def test_zk_select_scada(self):
    """Select_SCADA"""
    browser = self.browser
    action = ActionChains(browser)
    scda = browser.find_element_by_xpath('//div[contains(@class, "form-field \
form-field-SCADA_COMM_MEDIUM modified present")]//div[contains(@class, \
"combobox-caret")]')
    scda.click()
    time.sleep(2)
    scdb = browser.find_element_by_xpath('//*[@id="id-container"]/div[1]/a[1]')
    scdb.click()
    for _i in range(2):
        action.send_keys(Keys.PAGE_DOWN).perform()
    print('test_zk_select_SCADA')


# def test_zl_select_all_tags(self):
    # """Select_ALL_TAGS"""
    # browser = self.browser
    # alltags = browser.find_element_by_xpath('/html/body/div/div[2]/div/\
# div[1]/div[2]/div/div[2]/div[2]/div[5]/a/span')
    # tag = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/\
# div[2]/div[2]/div[5]/div/ul/li[1]')
    # print('alltags = {0}'.format(alltags))
    # print('tag = {0}'.format(tag))
    # if tag.is_displayed():
    #     print('Tags are displayed')
    # # allt = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/\
# # div/div[2]/div[2]/div[5]/a/span')
    # else:
    #     print('Tags are NOT yet displayed')
    #     alltags.click()
    #     time.sleep(2)
    # print('test_zl_select_ALL_TAGS')


# def test_zm_select_plcop(self):
    # """Select_PLCOP"""
    # browser = self.browser
    # action = ActionChains(browser)
    # plcop = browser.find_element_by_xpath('/html/body/div/div[2]/div/\
# div[1]/div[2]/div/div[2]/div[2]/div[5]/div/ul/li[19]/div[2]/input')
    # # plcop = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/\
# div/div[2]/div[2]/div[5]/div/ul/li[20]/div[2]')
    # plcop.click()
    # time.sleep(1)
    # action.send_keys(Keys.BACKSPACE).perform()
    # action.send_keys("B").perform()
    # print('test_zm_select_PLCOP')


# def test_zn_select_record_status(self):
    # """Select_Record_STATUS"""
    # browser = self.browser
    # action = ActionChains(browser)
    # rsa = browser.find_element_by_xpath('/html/body/div/div[2]/div/div[1]/\
# div[2]/div/div[2]/div[2]/div[5]/div/ul/li[20]/div[2]/input')
    # rsa.click()
    # time.sleep(1)
    # action.send_keys(Keys.BACKSPACE).perform()
    # action.send_keys("A").perform()
    # print('test_zn_select_Record_Status')
    # time.sleep(1)
    # for i in range(7):
    #     action.send_keys(Keys.PAGE_UP).perform()
    #     time.sleep(.5)


####################
# Begin unit tests #
####################


class FishNetToMidb(unittest.TestCase):
    """set up class"""
    @classmethod
    def setUpClass(cls):
        # self.browser = webdriver.Firefox(FF_profile) #FireFox
        cls.browser = DRIVER  # Chrome

    @staticmethod
    def test_aa_iterator():
        """<FISHNet>"""
        stp = False
        while stp is False:
            user_input = input("Please enter number of geojson map points to \
create, range is 1 to 325. \"0\" to exit: ")
            try:
                kbi = int(user_input)
                if kbi < 0:
                    stp = False
                    print('Invalid: Enter number, range is 1 to 325, \"0\" \
to exit')
                elif kbi > 325:
                    stp = False
                    print('Invalid: Enter number, range is 1 to 325, \"0\" \
to exit')
                elif kbi == 0:
                    stp = True
                    print('Exiting...')
                    sys.exit([0])
                else:
                    stp = True
            except ValueError:
                stp = False

        FishNetToMidb.foo = kbi

    def test_a_login(self):
        """Login to FISHNet"""
        browser = self.browser
        browser.get('https://geoserver.fishnet-project.io/acknowledge')
        # browser.set_window_position(2000,0)
        browser.maximize_window()
        # ipdb.set_trace()
        # browser.implicitly_wait(8) # Need to pause the webdriver for the \
# page to fully load otherwis element not found
        button = browser.find_element_by_xpath('//form[contains(@class, "ui \
form")]/button[contains(@type, "submit")]')
        button.click()
        button2 = browser.find_element_by_xpath('//div[contains(@class, \
"modal-actions cf")]/button[contains(@class, "start-editing col6")]')
        button2.click()
        browser.get('https://geoserver.fishnet-project.io/#background=OSM%20\
Basemap&disable_features=midb&map=16.00/37.3230/105.5964')  # site 1

    def test_b_search_for_title(self):
        """Title check, Small test can keep or throw away"""
        browser = self.browser
        self.assertIn("FISHNet", browser.title)

    def test_c_select_map_data_disc(self):
        """Select discipline"""
        browser = self.browser
        # wait = WebDriverWait(self.browser, 10) # Needed to add this wait in \
# this mannor as the page is not loading consistantly.
        button = browser.find_element_by_xpath('//div[contains(@class, \
"map-control map-data-control")]/button')
        button.click()
        time.sleep(1)
        # wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/\
# div/div[2]/div/div[2]/div[1]/div[5]/div[3]/div/div[2]/div[1]/div/\
# ul/li[4]/label/span")))
        buttondis = browser.find_element_by_xpath('//div[contains(@class, \
"map-data-disciplines")]//div[contains(@class, "disclosure-wrap \
disclosure-wrap-data_disciplines")]/ul[contains(@class, "layer-list \
layer-discipline-list")]/li[4]/label/span')
        buttondis.click()
        time.sleep(1)
        # closeMDmenue = browser.find_element_by_xpath('//div[contains\
# (@class, "map-control map-data-control")]/button').click()

    def test_d_repeat(self):
        """Repeat marking points"""
        counter = 0
        while counter < FishNetToMidb.foo:
            ap_ep_npp(self)
            counter = counter + 1
            print('Point {0} of {1} Completed...'.format
                  (counter, FishNetToMidb.foo))

    def test_zo_select_save(self):
        """Save and Upload"""
        browser = self.browser
        sva = browser.find_element_by_xpath('//div[contains(@class, \
"button-wrap col1")]/button[contains(@class, "save col12 has-count")]')
        sva.click()
        time.sleep(1)
        svb = browser.find_element_by_xpath('//div[contains(@class, "buttons \
fillL cf")]/button[contains(@class, "action col5 button save-button")]')
        svb.click()
        time.sleep(2)

#################
# Close Browser #
#################

    def test_zp_teardown(self):
        """Close Browser"""
        # browser = self.browser
        # wait = WebDriverWait(browser, 8) # Note this tells the browser to \
# wait UPTO, not fixed
        # waitelement = wait.until(EC.element_to_be_clickable((By.XPATH, '//\
# div[contains(@class, "ui large secondary pointing menu")]//a[contains\
# (@class, "item active")]')))
        self.browser.close()


#################
# Main Function #
#################


if __name__ == "__main__":
    unittest.main(verbosity=2)

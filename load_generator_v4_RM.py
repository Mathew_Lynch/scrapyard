"""Load Generator - simulating a rest client and creates payload
Generate geojson SQS message for input into MIDB, current limit is rougly \
325 messages due to SQS limitation of 256kb
Discipline is EP
Feature is Nuclear Power plants
Note:  This script assumes you have working cert in pem format with password.
"""
import datetime
from io import open
import json
import time
import random
import sys
import requests

##################################
# Define variables and functions #
##################################

CORDX = ['6', '7', '8']
CORDY = ['1', '2', '3']


def make_time():  # Supports midb time format
    """make some time"""
    time_now = datetime.datetime.now()
    stime_now = time_now.strftime('%Y-%m-%dT%H:%M:%S.%f')
    ftime_now = stime_now[:-3]
    return str("{0}Z".format(ftime_now))


def make_fname():  # Supports randomizing name
    """make facility name"""
    return str(random.randint(1, 10000))


def make_lname():  # Supports randomizing name
    """make facility name"""
    return str(random.randint(1, 10000))


def make_xcord():
    """make x coordinates"""
    return str(random.randint(1, 100000000000))


def make_ycord():
    """make y coordinates"""
    return str(random.randint(1, 100000000000))


# Note: Coordinates are predetermined by hand. The 3rd digits are randomized \
# and limited to keep the points within a rough boundry of the target
def iterator():
    """build the geojson"""
    midbtime = make_time()
    jeanluc = make_fname()
    picarad = make_lname()
    lata = random.choice(CORDX)
    latb = make_xcord()
    lona = random.choice(CORDY)
    lonb = make_ycord()
    coordz = ('"coordinates": [121.63{0}{1}, 29.10{2}{3}],'
              .format(lata, latb, lona, lonb))
    facname = ('"fac_name": "CN NPP A{0}T{1}",'.format(jeanluc, picarad))
    dtc = ('"datetime_created": "{0}",'.format(midbtime))
    dtlc = ('"datetime_last_chg": "{0}",'.format(midbtime))
    revd = ('"review_date": "{0}",'.format(midbtime))
    # combo = ('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}'.format(A, coordz, B, facname, C,
    #                                                 dtlc, revd, D, dtc, E))
    combo = ('{0}{1}{2}'.format(A, coordz, E))
    return combo

########
# MAIN #
########

###################
# Validate Input  #
###################


CERT = ('./BBpem.pem')  # pem file with password created as node, this may
# have to be handled differently High side
HEADERS = {'MIDB-Client-ID': 'KODIAK'}


def main():
    """main function"""
    stp = False
    while stp is False:
        user_input = input("Please enter number of geojson map points \
to create, range is 1 to 325. \"0\" to exit: ")
        try:
            kbi = int(user_input)
            if kbi < 0 or kbi > 325:
                stp = False
                print('Invalid: Enter number, range is 1 to 325, \"0\" to \
exit')
            elif kbi == 0:
                stp = True
                print('Exiting...')
                sys.exit()
            else:
                stp = True
                file = open('./lgen.json', 'w', encoding='utf-8')
                file.write('{"features": [')
        except ValueError:
            stp = False
            print('Invalid: Enter number, range is 1 to 325, \"0\" to \
exit')

####################
# Generate geojson #
####################

    tgt = 0
    while tgt < kbi:
        runs = iterator()
        print(runs)
        file.write(str(runs))
        tgt = tgt + 1
        if tgt == kbi:
            file.write('}],"type":"FeatureCollection"}')
            file.close()
        else:
            file.write(F)
            time.sleep(.001)

#################################
# post geojson to message queue #
#################################
    with open('./lgen.json', 'r') as ftarget:
        jsondata = json.load(ftarget)
    ftarget.close()
    #payload = jsondata
    payload = {}
    print("")
    print('###########################################################')
    print('########                                     ##############')
    print('########    POSTING TO MIDB AUTOMATION API   ##############')
    print('########                                     ##############')
    print('###########################################################')
    requesting = requests.post('https://api.midbautomation.com/midb/1.0/jobs',
                               data=json.dumps(payload), cert=CERT,
                               headers=HEADERS)
    print(requesting.encoding)
    print(requesting.text)
    print("")
    print('###########################################################')
    print('########                                     ##############')
    print('########            POST Successful!         ##############')
    print('########                                     ##############')
    print('###########################################################')

##################
# Body of geojson#
##################


# Note the PKI information would need to change for a different cert
# or moving highside
A = '{"geometry":{'
B = '"type": "Point"},"id": "nuclearpowerplants.undefined","type": "Feature" \
,"properties": {"cc": "CH","coord_deriv": "AG","prod_lvl_cap": \
"S", "activity": "AAC", "codeword": "0", "role_type": "NATN", \
"record_status": "A","domain_lvl": "CO","class_lvl": "U","discipline": \
"EP", "oper_status": "U", "control_mark": "IM", "feature_type": \
"nuclearpowerplants", '
C = '"condition": "UNC", "release_mark": "BW", '
D = '"eval": "1", "last_chg_userid": "bigbear2", '
E = '"placement": "ACHB","category": "42140","scada_comm_medium": "0"}, \
"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, \
ST=Virginia, C=US"'
F = '},'

if __name__ == "__main__":
    main()

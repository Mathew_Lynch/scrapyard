#Generate geojson SQS message for input into MIDB, current limit is rougly 325 messages due to SQS limitation of 256kb
#Discipline is EP
#Feature is Nuclear Power plants
#Note:  This script assumes you have working cert in pem format with password.
import datetime
import time
import random
import sys
import requests
import json
from io import open

################################
#Define variables and functions#
################################

cordx = ['6', '7', '8']
cordy = ['1', '2', '3']

def makeTime(): #Supports midb time format
	t = datetime.datetime.now()
	s = t.strftime('%Y-%m-%dT%H:%M:%S.%f')
	u = s[:-3]
	return str("{0}Z".format(u))

def makeFName(): #Supports randomizing name
	return str(random.randint(1,10000)) 
	
def makeLName(): #Supports randomizing name
	return str(random.randint(1,10000))

def makeXcord():
	return str(random.randint(1,100000000000))

def makeYcord():
	return str(random.randint(1,100000000000))

#Note: Coordinates are predetermined by hand. The 3rd digits are randomized and limited to keep the points within a rough boundry of the target 
def iterAtor():
	midbTime = makeTime()
	jeanluc = makeFName()
	picarad = makeLName()
	lata = random.choice(cordx)
	latb = makeXcord()
	lona = random.choice(cordy)
	lonb = makeYcord()
	coordz = ('"coordinates": [121.63{0}{1}, 29.10{2}{3}],'.format(lata,latb,lona,lonb))
	facName = ('"fac_name": "CN NPP A{0}T{1}",'.format(jeanluc,picarad))
	dtc = ('"datetime_created": "{0}",'.format(midbTime))
	dtlc = ('"datetime_last_chg": "{0}",'.format(midbTime))
	revd = ('"review_date": "{0}",'.format(midbTime))
	combo = ('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}'.format(a, coordz, b, facName, c, dtlc, revd, d, dtc, e))
	return combo

		########
		# MAIN #
		########

###################
# Validate Input  #
###################
def main():
	stp = False
	while stp == False:
		user_input = input("Please enter number of geojson map points to create, range is 1 to 325. \"0\" to exit: ")
		try:
			stp = int(user_input)
			if stp < 0:
				stp = False
				print('Invalid: Enter number, range is 1 to 325, \"0\" to exit')
			elif stp > 325:
				stp = False
				print('Invalid: Enter number, range is 1 to 325, \"0\" to exit')
			elif stp == 0:
				stp = True
				print('Exiting...')
				sys.exit()
			else:
				file = open('./lgen.json', 'w', encoding='utf-8')
				file.write('{"features": [')
		except ValueError:
			stp = False

###################
#Generate geojson #
###################
			
	tgt = 0
	while tgt < stp:
			x = iterAtor()
			print(x)
			file.write(str(x))
			tgt = tgt + 1
			if tgt == stp:
					file.write('}],"type":"FeatureCollection"}')
					file.close()
			else:
					file.write(f)
			time.sleep(.001)

#################################				
# post geojson to message queue #
#################################

	cert = ('./BBpem.pem') #pem file with password created as node, this may have to be handled differently High side
	headers = {'MIDB-Client-ID': 'FISHNet'}
	with open('./lgen.json', 'r') as q:
		jsondata=json.load(q)
	q.close()
	payload=jsondata
	print("")
	print('###########################################################')
	print('########                                     ##############')   
	print('########    POSTING TO MIDB AUTOMATION API   ##############')
	print('########                                     ##############')
	print('###########################################################')
	r = requests.post('https://api.midbautomation.com/midb/1.0/jobs', data=json.dumps(payload), cert=cert, headers=headers)
	print(r.encoding)
	print(r.text)
	print("")
	print('###########################################################')
	print('########                                     ##############')   
	print('########            POST Successful!         ##############')
	print('########                                     ##############')
	print('###########################################################')

#################
#Body of geojson#
#################

#Note the PKI information would need to change for a different cert or moving highside
a = '{"geometry":{'  
b = '"type": "Point"},"id": "nuclearpowerplants.undefined","type": "Feature","properties": {"cc": "CH","coord_deriv": "AG","prod_lvl_cap": "S","activity": "AAC","codeword": "0","role_type": "NATN","record_status": "A","domain_lvl": "CO","class_lvl": "U","discipline": "EP","oper_status": "U","control_mark": "IM","feature_type": "nuclearpowerplants",'
c = '"condition": "UNC","release_mark": "BW",'
d = '"eval": "1","last_chg_userid": "bigbear2",'
e = '"placement": "ACHB","category": "42140","scada_comm_medium": "0"},"PKI_SUBJECT_DN": "CN=bigbear2, OU=MIDB, O=NGC, L=Herndon, ST=Virginia, C=US"'
f = '},'

if __name__ == "__main__":
    main()

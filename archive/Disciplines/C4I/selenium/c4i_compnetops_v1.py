#Todo: Incorporate other disciplines for payload testing.
#
#import ipdb
import datetime
import random
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

#from repeat import add_points
#import repeat

##############################
#Tell FF which profile to use#
##############################

#FF_profile = webdriver.FirefoxProfile('C:/Users/Mlynch/AppData/Roaming/Mozilla/Firefox/Profiles/om5v57j2.selentstr') # Firefox
driver = webdriver.Chrome('C:/Users/Mlynch/Downloads/chromedriver_win32/chromedriver') #chrome

def ap_EP_NPP(self):
    test_g_markPoint(self)
    test_h_Select_FeatureType(self)
    test_ha_Select_Capacity(self)
    test_hb_Select_CapEval(self)
    test_hc_Select_CapEvalUM(self)
    test_i_Select_CountryCode(self)
    test_j_Select_classlvl(self)
    test_k_Select_Phys_Con(self)
    test_l_Select_DateTime_C(self)
    test_m_Select_DateTime_LC(self)
    test_ma_Select_EPCC(self)
    test_n_Select_Confidence(self)
    test_o_Select_Facility_Name(self)
    test_v_Select_LCUID(self)
    test_x_Select_OS(self)
#    test_y_Select_PLCOP(self)
#    test_z_Select_RS(self)
    test_za_Select_BCT(self)
    test_zb_Select_BCTUM(self)
    test_zc_Select_RIMin(self)
    test_zd_Select_RIMax(self)
    test_ze_Select_RIUM(self)
    test_zf_Select_RLI(self)
    test_zg_Select_RRP(self)
    test_zh_Select_RM(self)
    test_zi_Select_ReviewDate(self)
    test_zj_Select_RoleType(self)
    test_zk_Select_SCADA(self)
    test_zl_Select_ALL_TAGS(self)
    test_zm_Select_PLCOP(self)
    test_zn_Select_Record_STATUS(self)
    
def test_g_markPoint(self):  #Place Marker Point
	browser = self.browser
	button = browser.find_element_by_xpath('//div[contains(@class, "button-wrap joined col3")]//button[1]')
	button.click()
	action = ActionChains(browser)
	x = random.randint(-60,60)
	#xoffset = x + 520 #First point
	xoffset = x + 350
	y = random.randint(-60,60)
	#yoffset = y + 384 #First Point
	yoffset = y + 334
	clickMap = browser.find_element_by_xpath('//div[contains(@class, "layer layer-imagery")]')
	#action.move_to_element_with_offset(clickMap, 384, 880).click().perform() #Center point of map #Full screen Portrait
	#action.move_to_element_with_offset(clickMap, 550, 384).click().perform() #Center point of map #Full screen landscape laptop
	action.move_to_element_with_offset(clickMap, xoffset, yoffset).click().perform() #Custom point center
	time.sleep(2)
	print('test_g_markPoint')
		
		
def test_h_Select_FeatureType(self):  #Edit the features about the point
	browser = self.browser
	taps = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/div[1]/div[2]/div/div[8]/div[1]/button/div[3]')
	taps.click()
	time.sleep(1)
	print('test_h_Select_FeatureTyp')
		
def test_ha_Select_Capacity(self):
	browser = self.browser
	cap = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CAPACITY_1 modified present")]//button[contains(@title, "remove")]')
	cap.click()
	print('test_ha_Select_Capacity')
	
def test_hb_Select_CapEval(self):
	browser = self.browser
	cape = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CAPACITY_EVAL modified present")]//button[contains(@title, "remove")]')
	cape.click()
	print('test_hb_Select_CapEval')

def test_hc_Select_CapEvalUM(self):
	browser = self.browser
	capeum = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CAPACITY_UM_0 modified present")]//button[contains(@title, "remove")]')
	capeum.click()
	print('test_hc_Select_CapEvalUM')
	
def test_i_Select_CountryCode(self): 
	browser = self.browser
	clla = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CLASS_LVL modified present")]') #Bring into view
	clla.click()
	cdw = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CODEWORD modified present")]')
	cdw.click()
	cca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CC_0")]//div[contains(@class, "combobox-caret")]')
	cca.click()
	cc1 = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "China")]')
	cc1.click()
	print('test_i_Select_CountryCode')
	
def test_j_Select_classlvl(self):
	browser = self.browser
	cdw = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CODEWORD modified present")]')
	cdw.click()
	physCona = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CONDITION_2 modified present")]')
	physCona.click()
	clla = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CLASS_LVL modified present")]//div[contains(@class, "combobox-caret")]')
	clla.click()
	cllb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unclassified")]')
	cllb.click()
	print('test_j_Select_classlvl')
	
def test_k_Select_Phys_Con(self):
	browser = self.browser
	physCona = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-CONDITION_2 modified present")]//div[contains(@class, "combobox-caret")]')
	physCona.click()
	physConb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Target Status Unknown; CPD is Greater Than 0% and Less Than 10%")]')
	physConb.click()
	print('test_k_Select_Phys_Con')
			
def test_l_Select_DateTime_C(self):
	t = datetime.datetime.now()
	s = t.strftime('%Y-%m-%dT%H:%M:%S.%f')
	u = s[:-3]
	midbTime = str("{0}Z".format(u))
	browser = self.browser
	action = ActionChains(browser)
	dtca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-DATETIME_CREATED")]')
	dtca.click()
	dtc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME_CREATED"]')
	dtc1.click()
	action.send_keys(midbTime).perform()
	time.sleep(1)
	print('test_l_Select_DateTime_C')
		
def test_m_Select_DateTime_LC(self):
	t = datetime.datetime.now()
	s = t.strftime('%Y-%m-%dT%H:%M:%S.%f')
	u = s[:-3]
	midbTime = str("{0}Z".format(u))
	browser = self.browser
	action = ActionChains(browser)
	dtlca = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-DATETIME_LAST_CHG")]')
	dtlca.click()
	dtlc1 = browser.find_element_by_xpath('//*[@id="preset-input-DATETIME_LAST_CHG"]')
	dtlc1.click()
	action.send_keys(midbTime).perform()
	time.sleep(1)
	print('test_m_Select_DateTime_LC')
		
def test_ma_Select_EPCC(self):
	browser = self.browser   
	epcc = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-ELECTRIC_POWER_CONTROL_CENTER modified present")]//button[contains(@title, "remove")]')
	epcc.click()
	time.sleep(1)
	print('test_ma_Select_EPCC')
	
def test_n_Select_Confidence(self):
	browser = self.browser
	confa = browser.find_element_by_css_selector('.form-field-EVAL_3 > div:nth-child(3)')
	confa.click()
	time.sleep(2)
	confb = browser.find_element_by_css_selector('#id-container > div.combobox > a:nth-child(1)')
	confb.click()
	print('test_n_Select_Confidence')
	
def test_o_Select_Facility_Name(self):
	jeanluc = str(random.randint(1,10000)) 
	picard = str(random.randint(1,10000))
	browser = self.browser
	action = ActionChains(browser)
	fac_namea = browser.find_element_by_css_selector('.form-field-FAC_NAME > label:nth-child(1) > div:nth-child(1)')
	fac_namea.click()
	fac_nameb = browser.find_element_by_css_selector('#preset-input-FAC_NAME')
	fac_nameb.click()
	for i in range(3):
		action.send_keys(Keys.BACKSPACE).perform()
	action.send_keys("CN Comp NetOps A{0}T{1}".format(jeanluc,picard)).perform()
	print('test_o_Select_Facility_Name')
		
def test_v_Select_LCUID(self):
	browser = self.browser
	action = ActionChains(browser)        
	lcuid = browser.find_element_by_xpath('//*[@id="preset-input-LAST_CHG_USERID"]')
	lcuid.click()
	time.sleep(2)
	action.send_keys("bigbear2").perform()
	time.sleep(2)
	print('test_v_Select_LCUID')
	
def test_x_Select_OS(self):
	browser = self.browser
	osa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-OPER_STATUS_1 modified present")]//div[contains(@class, "combobox-caret")]')
	osa.click()
	osb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unknown")]')
	osb.click()
	print('test_x_Select_OS')

#Commented out the below methods because selenium cannot view the element, perhaps because the element is dynamicly created when the combobox carret is clicked?  I don't know.
# methods zl, sm, zn replace them.
 	
# def test_y_Select_PLCOP(self):
	# browser = self.browser
	# plcopa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-PROD_LVL_CAP_1 modified present")]//div[contains(@class, "combobox-caret")]')
	# plcopa.click()
	# #time.sleep(2)
	# plcopb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "BASIC")]')
	# plcopb.click()
	# print('test_y_Select_PLCOP')
	
# def test_z_Select_RS(self):
	# browser = self.browser
	# rsa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECORD_STATUS modified present")]//div[contains(@class, "combobox-caret")]')
	# rsa.click()
	# rsb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Active")]')
	# rsb.click()
	# print('test_z_Select_RS')
	
def test_za_Select_BCT(self):
	browser = self.browser
	bcta = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_BKUP_CAP_TIME modified present")]//button[contains(@title, "remove")]')
	bcta.click()
	print('test_za_Select_BCT')

def test_zb_Select_BCTUM(self):
	browser = self.browser
	bctuma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_BKUP_CAP_TIME_UOM modified present")]//button[contains(@title, "remove")]')
	bctuma.click()
	print('test_zb_Select_BCTUM')

def test_zc_Select_RIMin(self):
	browser = self.browser
	rimna = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL modified present")]//button[contains(@title, "remove")]')
	rimna.click()
	print('test_zc_Select_RIMin')

def test_zd_Select_RIMax(self):
	browser = self.browser
	rimxa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL_MAX modified present")]//button[contains(@title, "remove")]')
	rimxa.click()
	print('test_zd_Select_RIMax')

def test_ze_Select_RIUM(self):
	browser = self.browser
	riuma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_INTRVL_UM_1 modified present")]//button[contains(@title, "remove")]')
	riuma.click()
	print('test_ze_Select_RIUM')

def test_zf_Select_RLI(self):
	browser = self.browser
	rlia = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_LOSS_IMPACT modified present")]//button[contains(@title, "remove")]')
	rlia.click()
	print('test_zf_Select_RLI')

def test_zg_Select_RRP(self):
	browser = self.browser
	rrpa = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RECUP_REPAIR_PRIORITY_1 modified present")]//button[contains(@title, "remove")]')
	rrpa.click()
	print('test_zg_Select_RRP')

def test_zh_Select_RM(self):
	browser = self.browser
	rma = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-RELEASE_MARK modified present")]')
	rma.click()
	print('test_zh_Select_RM')

def test_zi_Select_ReviewDate(self):
	t = datetime.datetime.now()
	s = t.strftime('%Y-%m-%dT%H:%M:%S.%f')
	u = s[:-3]
	midbTime = str("{0}Z".format(u))
	browser = self.browser
	action = ActionChains(browser)
	rda = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-REVIEW_DATE")]')
	rda.click()
	rd1 = browser.find_element_by_xpath('//*[@id="preset-input-REVIEW_DATE"]')
	rd1.click()
	action.send_keys(midbTime).perform()
	time.sleep(2)
	print('test_zi_Select_ReviewDate')

def test_zj_Select_RoleType(self):
	browser = self.browser
	rta = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-ROLE_TYPE_1 modified present")]//div[contains(@class, "combobox-caret")]')
	rta.click()
	rtb = browser.find_element_by_xpath('//div[contains(@class, "combobox")]//a[contains(@title, "Unknown.")]')
	rtb.click()
	print('test_zj_Select_RoleType')

def test_zk_Select_SCADA(self):
	browser = self.browser
	action = ActionChains(browser)
	scda = browser.find_element_by_xpath('//div[contains(@class, "form-field form-field-SCADA_COMM_MEDIUM modified present")]//div[contains(@class, "combobox-caret")]')
	scda.click()
	time.sleep(2)
	scdb = browser.find_element_by_xpath('//*[@id="id-container"]/div[1]/a[1]')
	scdb.click()
	for i in range(2):
		action.send_keys(Keys.PAGE_DOWN).perform()
	print('test_zk_Select_SCADA')

def test_zl_Select_ALL_TAGS(self):
	browser = self.browser
	alltags = browser.find_element_by_xpath('/html/body/div/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div[5]/a/span')
	tag = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/div[2]/div[2]/div[5]/div/ul/li[1]')
	print('alltags = {0}'.format(alltags))
	print('tag = {0}'.format(tag))
	if tag.is_displayed():
		print('Tags are displayed')
	#allt = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/div[2]/div[2]/div[5]/a/span')
	else:
		print('Tags are NOT yet displayed')
		alltags.click()
		time.sleep(2)
	print('test_zl_Select_ALL_TAGS')

def test_zm_Select_PLCOP(self):
	browser = self.browser
	action = ActionChains(browser)
	plcop = browser.find_element_by_xpath('/html/body/div/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div[5]/div/ul/li[19]/div[2]/input')
	#plcop = browser.find_element_by_xpath('//*[@id="sidebar"]/div[2]/div/div[2]/div[2]/div[5]/div/ul/li[20]/div[2]')
	plcop.click()
	time.sleep(1)
	action.send_keys(Keys.BACKSPACE).perform()
	action.send_keys("B").perform()
	print('test_zm_Select_PLCOP')

def test_zn_Select_Record_STATUS(self):
	browser = self.browser
	action = ActionChains(browser)
	rsa = browser.find_element_by_xpath('/html/body/div/div[2]/div/div[1]/div[2]/div/div[2]/div[2]/div[5]/div/ul/li[20]/div[2]/input')
	rsa.click()
	time.sleep(1)
	action.send_keys(Keys.BACKSPACE).perform()
	action.send_keys("A").perform()
	print('test_zn_Select_Record_Status')
	time.sleep(1)
	for i in range(7):
		action.send_keys(Keys.PAGE_UP).perform()
		time.sleep(.5)
	
	
##################
#Begin unit tests#
##################

class FISHNetToMIDB_EP_Thermal_PP(unittest.TestCase):
	@classmethod
	def setUpClass(self):
		#self.browser = webdriver.Firefox(FF_profile) #FireFox
		self.browser = driver #Chrome

	def test_aa_iterator(self):
		stp = False
		while stp == False:
			user_input = input("Please enter number of geojson map points to create, range is 1 to 325. \"0\" to exit: ")
			try:
				stp = int(user_input)
				if stp < 0:
					stp = False
					print('Invalid: Enter number, range is 1 to 325, \"0\" to exit')
				elif stp > 325:
					stp = False
					print('Invalid: Enter number, range is 1 to 325, \"0\" to exit')
				elif stp == 0:
					stp = True
					print('Exiting...')
					sys.exit()
			except ValueError:
				stp = False
		FISHNetToMIDB_EP_Thermal_PP.foo = stp

	def test_a_login(self):  #Login to FISHNet
		browser = self.browser
		browser.get('https://geoserver.fishnet-project.io/acknowledge')
		browser.maximize_window()
		button = browser.find_element_by_xpath('//form[contains(@class, "ui form")]/button[contains(@type, "submit")]')
		button.click()
		button2 = browser.find_element_by_xpath('//div[contains(@class, "modal-actions cf")]/button[contains(@class, "start-editing col6")]') 
		button2.click()
		browser.get('https://geoserver.fishnet-project.io/#background=OSM%20Basemap&disable_features=midb&map=16.00/43.0297/36.3604')  #site 1
		

	def test_b_search_for_title(self):  #Small test can keep or throw away
		browser = self.browser
		self.assertIn("FISHNet", browser.title)

	def test_c_select_map_data_Disc(self):  #Will need to break out each discipline to test in their own set of cases
		browser = self.browser
		#wait = WebDriverWait(self.browser, 10) #Needed to add this wait in this mannor as the page is not loading consistantly.
		button = browser.find_element_by_xpath('//div[contains(@class, "map-control map-data-control")]/button')
		button.click()
		time.sleep(1)
		#wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div[2]/div/div[2]/div[1]/div[5]/div[3]/div/div[2]/div[1]/div/ul/li[4]/label/span")))       
		buttonDIS = browser.find_element_by_xpath('//div[contains(@class, "map-data-disciplines")]//div[contains(@class, "disclosure-wrap disclosure-wrap-data_disciplines")]/ul[contains(@class, "layer-list layer-discipline-list")]/li[4]/label/span')
		buttonDIS.click()
		time.sleep(1)
		closeMDmenue = browser.find_element_by_xpath('//div[contains(@class, "map-control map-data-control")]/button').click()

	def test_d_repeat(self): #Repeat marking points x number of times
		counter = 0
		while counter < FISHNetToMIDB_EP_Thermal_PP.foo:
			ap_EP_NPP(self)
			counter = counter + 1
			print('Point {0} of {1} Completed...'.format(counter,FISHNetToMIDB_EP_Thermal_PP.foo))

	def test_zo_Select_Save(self): #Save and Upload
		browser = self.browser
		sva = browser.find_element_by_xpath('//div[contains(@class, "button-wrap col1")]/button[contains(@class, "save col12 has-count")]')
		sva.click()
		time.sleep(1)
		svb = browser.find_element_by_xpath('//div[contains(@class, "buttons fillL cf")]/button[contains(@class, "action col5 button save-button")]')
		svb.click()
		time.sleep(2)

###############
#Close Browser#
###############

	def test_zp_tearDown(self):
		browser = self.browser
		wait = WebDriverWait(browser, 8) #Note this tells the browser to wait UPTO, not fixed
		waitelement = wait.until(EC.element_to_be_clickable((By.XPATH, '//div[contains(@class, "ui large secondary pointing menu")]//a[contains(@class, "item active")]')))
		self.browser.close()

        
###############
#Main Function#
###############

if __name__ == "__main__":
	unittest.main(verbosity=2)
